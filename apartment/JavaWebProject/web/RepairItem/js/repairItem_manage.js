﻿var repairItem_manage_tool = null; 
$(function () { 
	initRepairItemManageTool(); //建立RepairItem管理对象
	repairItem_manage_tool.init(); //如果需要通过下拉框查询，首先初始化下拉框的值
	$("#repairItem_manage").datagrid({
		url : 'RepairItem/list',
		fit : true,
		fitColumns : true,
		striped : true,
		rownumbers : true,
		border : false,
		pagination : true,
		pageSize : 5,
		pageList : [5, 10, 15, 20, 25],
		pageNumber : 1,
		sortName : "itemId",
		sortOrder : "desc",
		toolbar : "#repairItem_manage_tool",
		columns : [[
			{
				field : "itemId",
				title : "项目id",
				width : 70,
			},
			{
				field : "itemName",
				title : "项目名称",
				width : 140,
			},
		]],
	});

	$("#repairItemEditDiv").dialog({
		title : "修改管理",
		top: "50px",
		width : 700,
		height : 515,
		modal : true,
		closed : true,
		iconCls : "icon-edit-new",
		buttons : [{
			text : "提交",
			iconCls : "icon-edit-new",
			handler : function () {
				if ($("#repairItemEditForm").form("validate")) {
					//验证表单 
					if(!$("#repairItemEditForm").form("validate")) {
						$.messager.alert("错误提示","你输入的信息还有错误！","warning");
					} else {
						$("#repairItemEditForm").form({
						    url:"RepairItem/" + $("#repairItem_itemId_edit").val() + "/update",
						    onSubmit: function(){
								if($("#repairItemEditForm").form("validate"))  {
				                	$.messager.progress({
										text : "正在提交数据中...",
									});
				                	return true;
				                } else { 
				                    return false; 
				                }
						    },
						    success:function(data){
						    	$.messager.progress("close");
						    	console.log(data);
			                	var obj = jQuery.parseJSON(data);
			                    if(obj.success){
			                        $.messager.alert("消息","信息修改成功！");
			                        $("#repairItemEditDiv").dialog("close");
			                        repairItem_manage_tool.reload();
			                    }else{
			                        $.messager.alert("消息",obj.message);
			                    } 
						    }
						});
						//提交表单
						$("#repairItemEditForm").submit();
					}
				}
			},
		},{
			text : "取消",
			iconCls : "icon-redo",
			handler : function () {
				$("#repairItemEditDiv").dialog("close");
				$("#repairItemEditForm").form("reset"); 
			},
		}],
	});
});

function initRepairItemManageTool() {
	repairItem_manage_tool = {
		init: function() {
		},
		reload : function () {
			$("#repairItem_manage").datagrid("reload");
		},
		redo : function () {
			$("#repairItem_manage").datagrid("unselectAll");
		},
		search: function() {
			$("#repairItem_manage").datagrid("load");
		},
		exportExcel: function() {
			$("#repairItemQueryForm").form({
			    url:"RepairItem/OutToExcel",
			});
			//提交表单
			$("#repairItemQueryForm").submit();
		},
		remove : function () {
			var rows = $("#repairItem_manage").datagrid("getSelections");
			if (rows.length > 0) {
				$.messager.confirm("确定操作", "您正在要删除所选的记录吗？", function (flag) {
					if (flag) {
						var itemIds = [];
						for (var i = 0; i < rows.length; i ++) {
							itemIds.push(rows[i].itemId);
						}
						$.ajax({
							type : "POST",
							url : "RepairItem/deletes",
							data : {
								itemIds : itemIds.join(","),
							},
							beforeSend : function () {
								$("#repairItem_manage").datagrid("loading");
							},
							success : function (data) {
								if (data.success) {
									$("#repairItem_manage").datagrid("loaded");
									$("#repairItem_manage").datagrid("load");
									$("#repairItem_manage").datagrid("unselectAll");
									$.messager.show({
										title : "提示",
										msg : data.message
									});
								} else {
									$("#repairItem_manage").datagrid("loaded");
									$("#repairItem_manage").datagrid("load");
									$("#repairItem_manage").datagrid("unselectAll");
									$.messager.alert("消息",data.message);
								}
							},
						});
					}
				});
			} else {
				$.messager.alert("提示", "请选择要删除的记录！", "info");
			}
		},
		edit : function () {
			var rows = $("#repairItem_manage").datagrid("getSelections");
			if (rows.length > 1) {
				$.messager.alert("警告操作！", "编辑记录只能选定一条数据！", "warning");
			} else if (rows.length == 1) {
				$.ajax({
					url : "RepairItem/" + rows[0].itemId +  "/update",
					type : "get",
					data : {
						//itemId : rows[0].itemId,
					},
					beforeSend : function () {
						$.messager.progress({
							text : "正在获取中...",
						});
					},
					success : function (repairItem, response, status) {
						$.messager.progress("close");
						if (repairItem) { 
							$("#repairItemEditDiv").dialog("open");
							$("#repairItem_itemId_edit").val(repairItem.itemId);
							$("#repairItem_itemId_edit").validatebox({
								required : true,
								missingMessage : "请输入项目id",
								editable: false
							});
							$("#repairItem_itemName_edit").val(repairItem.itemName);
							$("#repairItem_itemName_edit").validatebox({
								required : true,
								missingMessage : "请输入项目名称",
							});
						} else {
							$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
						}
					}
				});
			} else if (rows.length == 0) {
				$.messager.alert("警告操作！", "编辑记录至少选定一条数据！", "warning");
			}
		},
	};
}
