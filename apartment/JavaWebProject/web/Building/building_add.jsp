﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/building.css" />
<div id="buildingAddDiv">
	<form id="buildingAddForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">公寓楼名称:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="building_buildingName" name="building.buildingName" style="width:200px" />

			</span>

		</div>
		<div>
			<span class="label">公寓楼类型:</span>
			<span class="inputControl">
				<select id="building_buildingType" name="building.buildingType">
					<option value="男">男</option>
					<option value="女">女</option>
					<option value="男女混">男女混</option>
				</select>
				
			</span>

		</div>
		<div>
			<span class="label">公寓楼介绍:</span>
			<span class="inputControl">
				<textarea id="building_buildingDesc" name="building.buildingDesc" rows="6" cols="80"></textarea>

			</span>

		</div>
		<div class="operation">
			<a id="buildingAddButton" class="easyui-linkbutton">添加</a>
			<a id="buildingClearButton" class="easyui-linkbutton">重填</a>
		</div> 
	</form>
</div>
<script src="${pageContext.request.contextPath}/Building/js/building_add.js"></script> 
