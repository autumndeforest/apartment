﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Building" %>
<%@ page import="com.org.po.Student" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
<title>住宿添加</title>
<link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
<link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
<link href="<%=basePath %>plugins/animate.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body style="margin-top:70px;">
<jsp:include page="../header.jsp"></jsp:include>
<div class="container">
	<div class="row">
		<div class="col-md-12 wow fadeInUp" data-wow-duration="0.5s">
			<div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
			    	<li role="presentation" ><a href="<%=basePath %>Live/frontlist">住宿列表</a></li>
			    	<li role="presentation" class="active"><a href="#liveAdd" aria-controls="liveAdd" role="tab" data-toggle="tab">添加住宿</a></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
				    <div role="tabpanel" class="tab-pane" id="liveList">
				    </div>
				    <div role="tabpanel" class="tab-pane active" id="liveAdd"> 
				      	<form class="form-horizontal" name="liveAddForm" id="liveAddForm" enctype="multipart/form-data" method="post"  class="mar_t15">
						  <div class="form-group">
						  	 <label for="live_buildingObj_buildingId" class="col-md-2 text-right">入住公寓楼:</label>
						  	 <div class="col-md-8">
							    <select id="live_buildingObj_buildingId" name="live.buildingObj.buildingId" class="form-control">
							    </select>
						  	 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="live_roomNo" class="col-md-2 text-right">入住宿舍号:</label>
						  	 <div class="col-md-8">
							    <input type="text" id="live_roomNo" name="live.roomNo" class="form-control" placeholder="请输入入住宿舍号">
							 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="live_studentObj_studentNo" class="col-md-2 text-right">入住学生:</label>
						  	 <div class="col-md-8">
							    <select id="live_studentObj_studentNo" name="live.studentObj.studentNo" class="form-control">
							    </select>
						  	 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="live_inDateDiv" class="col-md-2 text-right">入住日期:</label>
						  	 <div class="col-md-8">
				                <div id="live_inDateDiv" class="input-group date live_inDate col-md-12" data-link-field="live_inDate" data-link-format="yyyy-mm-dd">
				                    <input class="form-control" id="live_inDate" name="live.inDate" size="16" type="text" value="" placeholder="请选择入住日期" readonly>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				                </div>
						  	 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="live_liveMemo" class="col-md-2 text-right">备注信息:</label>
						  	 <div class="col-md-8">
							    <textarea id="live_liveMemo" name="live.liveMemo" rows="8" class="form-control" placeholder="请输入备注信息"></textarea>
							 </div>
						  </div>
				          <div class="form-group">
				             <span class="col-md-2""></span>
				             <span onclick="ajaxLiveAdd();" class="btn btn-primary bottom5 top5">添加</span>
				          </div>
						</form> 
				        <style>#liveAddForm .form-group {margin:10px;}  </style>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

<jsp:include page="../footer.jsp"></jsp:include> 
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script>
var basePath = "<%=basePath%>";
	//提交添加住宿信息
	function ajaxLiveAdd() { 
		//提交之前先验证表单
		$("#liveAddForm").data('bootstrapValidator').validate();
		if(!$("#liveAddForm").data('bootstrapValidator').isValid()){
			return;
		}
		jQuery.ajax({
			type : "post",
			url : basePath + "Live/add",
			dataType : "json" , 
			data: new FormData($("#liveAddForm")[0]),
			success : function(obj) {
				if(obj.success){ 
					alert("保存成功！");
					$("#liveAddForm").find("input").val("");
					$("#liveAddForm").find("textarea").val("");
				} else {
					alert(obj.message);
				}
			},
			processData: false, 
			contentType: false, 
		});
	} 
$(function(){
	/*小屏幕导航点击关闭菜单*/
    $('.navbar-collapse a').click(function(){
        $('.navbar-collapse').collapse('hide');
    });
    new WOW().init();
	//验证住宿添加表单字段
	$('#liveAddForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			"live.roomNo": {
				validators: {
					notEmpty: {
						message: "入住宿舍号不能为空",
					}
				}
			},
			"live.inDate": {
				validators: {
					notEmpty: {
						message: "入住日期不能为空",
					}
				}
			},
		}
	}); 
	//初始化入住公寓楼下拉框值
	$.ajax({
		url: basePath + "Building/listAll",
		type: "get",
		success: function(buildings,response,status) { 
			$("#live_buildingObj_buildingId").empty();
			var html="";
    		$(buildings).each(function(i,building){
    			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
    		});
    		$("#live_buildingObj_buildingId").html(html);
    	}
	});
	//初始化入住学生下拉框值 
	$.ajax({
		url: basePath + "Student/listAll",
		type: "get",
		success: function(students,response,status) { 
			$("#live_studentObj_studentNo").empty();
			var html="";
    		$(students).each(function(i,student){
    			html += "<option value='" + student.studentNo + "'>" + student.name + "</option>";
    		});
    		$("#live_studentObj_studentNo").html(html);
    	}
	});
	//入住日期组件
	$('#live_inDateDiv').datetimepicker({
		language:  'zh-CN',  //显示语言
		format: 'yyyy-mm-dd',
		minView: 2,
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		minuteStep: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
	}).on('hide',function(e) {
		//下面这行代码解决日期组件改变日期后不验证的问题
		$('#liveAddForm').data('bootstrapValidator').updateStatus('live.inDate', 'NOT_VALIDATED',null).validateField('live.inDate');
	});
})
</script>
</body>
</html>
