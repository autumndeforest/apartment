﻿var live_manage_tool = null; 
$(function () { 
	initLiveManageTool(); //建立Live管理对象
	live_manage_tool.init(); //如果需要通过下拉框查询，首先初始化下拉框的值
	$("#live_manage").datagrid({
		url : 'Live/list',
		fit : true,
		fitColumns : true,
		striped : true,
		rownumbers : true,
		border : false,
		pagination : true,
		pageSize : 5,
		pageList : [5, 10, 15, 20, 25],
		pageNumber : 1,
		sortName : "liveId",
		sortOrder : "desc",
		toolbar : "#live_manage_tool",
		columns : [[
			{
				field : "liveId",
				title : "记录id",
				width : 70,
			},
			{
				field : "buildingObj",
				title : "入住公寓楼",
				width : 140,
			},
			{
				field : "roomNo",
				title : "入住宿舍号",
				width : 140,
			},
			{
				field : "studentObj",
				title : "入住学生",
				width : 140,
			},
			{
				field : "inDate",
				title : "入住日期",
				width : 140,
			},
		]],
	});

	$("#liveEditDiv").dialog({
		title : "修改管理",
		top: "50px",
		width : 700,
		height : 515,
		modal : true,
		closed : true,
		iconCls : "icon-edit-new",
		buttons : [{
			text : "提交",
			iconCls : "icon-edit-new",
			handler : function () {
				if ($("#liveEditForm").form("validate")) {
					//验证表单 
					if(!$("#liveEditForm").form("validate")) {
						$.messager.alert("错误提示","你输入的信息还有错误！","warning");
					} else {
						$("#liveEditForm").form({
						    url:"Live/" + $("#live_liveId_edit").val() + "/update",
						    onSubmit: function(){
								if($("#liveEditForm").form("validate"))  {
				                	$.messager.progress({
										text : "正在提交数据中...",
									});
				                	return true;
				                } else { 
				                    return false; 
				                }
						    },
						    success:function(data){
						    	$.messager.progress("close");
						    	console.log(data);
			                	var obj = jQuery.parseJSON(data);
			                    if(obj.success){
			                        $.messager.alert("消息","信息修改成功！");
			                        $("#liveEditDiv").dialog("close");
			                        live_manage_tool.reload();
			                    }else{
			                        $.messager.alert("消息",obj.message);
			                    } 
						    }
						});
						//提交表单
						$("#liveEditForm").submit();
					}
				}
			},
		},{
			text : "取消",
			iconCls : "icon-redo",
			handler : function () {
				$("#liveEditDiv").dialog("close");
				$("#liveEditForm").form("reset"); 
			},
		}],
	});
});

function initLiveManageTool() {
	live_manage_tool = {
		init: function() {
			$.ajax({
				url : "Building/listAll",
				type : "post",
				success : function (data, response, status) {
					$("#buildingObj_buildingId_query").combobox({ 
					    valueField:"buildingId",
					    textField:"buildingName",
					    panelHeight: "200px",
				        editable: false, //不允许手动输入 
					});
					data.splice(0,0,{buildingId:0,buildingName:"不限制"});
					$("#buildingObj_buildingId_query").combobox("loadData",data); 
				}
			});
			$.ajax({
				url : "Student/listAll",
				type : "post",
				success : function (data, response, status) {
					$("#studentObj_studentNo_query").combobox({ 
					    valueField:"studentNo",
					    textField:"name",
					    panelHeight: "200px",
				        editable: false, //不允许手动输入 
					});
					data.splice(0,0,{studentNo:"",name:"不限制"});
					$("#studentObj_studentNo_query").combobox("loadData",data); 
				}
			});
		},
		reload : function () {
			$("#live_manage").datagrid("reload");
		},
		redo : function () {
			$("#live_manage").datagrid("unselectAll");
		},
		search: function() {
			var queryParams = $("#live_manage").datagrid("options").queryParams;
			queryParams["buildingObj.buildingId"] = $("#buildingObj_buildingId_query").combobox("getValue");
			queryParams["roomNo"] = $("#roomNo").val();
			queryParams["studentObj.studentNo"] = $("#studentObj_studentNo_query").combobox("getValue");
			queryParams["inDate"] = $("#inDate").datebox("getValue"); 
			$("#live_manage").datagrid("options").queryParams=queryParams; 
			$("#live_manage").datagrid("load");
		},
		exportExcel: function() {
			$("#liveQueryForm").form({
			    url:"Live/OutToExcel",
			});
			//提交表单
			$("#liveQueryForm").submit();
		},
		remove : function () {
			var rows = $("#live_manage").datagrid("getSelections");
			if (rows.length > 0) {
				$.messager.confirm("确定操作", "您正在要删除所选的记录吗？", function (flag) {
					if (flag) {
						var liveIds = [];
						for (var i = 0; i < rows.length; i ++) {
							liveIds.push(rows[i].liveId);
						}
						$.ajax({
							type : "POST",
							url : "Live/deletes",
							data : {
								liveIds : liveIds.join(","),
							},
							beforeSend : function () {
								$("#live_manage").datagrid("loading");
							},
							success : function (data) {
								if (data.success) {
									$("#live_manage").datagrid("loaded");
									$("#live_manage").datagrid("load");
									$("#live_manage").datagrid("unselectAll");
									$.messager.show({
										title : "提示",
										msg : data.message
									});
								} else {
									$("#live_manage").datagrid("loaded");
									$("#live_manage").datagrid("load");
									$("#live_manage").datagrid("unselectAll");
									$.messager.alert("消息",data.message);
								}
							},
						});
					}
				});
			} else {
				$.messager.alert("提示", "请选择要删除的记录！", "info");
			}
		},
		edit : function () {
			var rows = $("#live_manage").datagrid("getSelections");
			if (rows.length > 1) {
				$.messager.alert("警告操作！", "编辑记录只能选定一条数据！", "warning");
			} else if (rows.length == 1) {
				$.ajax({
					url : "Live/" + rows[0].liveId +  "/update",
					type : "get",
					data : {
						//liveId : rows[0].liveId,
					},
					beforeSend : function () {
						$.messager.progress({
							text : "正在获取中...",
						});
					},
					success : function (live, response, status) {
						$.messager.progress("close");
						if (live) { 
							$("#liveEditDiv").dialog("open");
							$("#live_liveId_edit").val(live.liveId);
							$("#live_liveId_edit").validatebox({
								required : true,
								missingMessage : "请输入记录id",
								editable: false
							});
							$("#live_buildingObj_buildingId_edit").combobox({
								url:"Building/listAll",
							    valueField:"buildingId",
							    textField:"buildingName",
							    panelHeight: "auto",
						        editable: false, //不允许手动输入 
						        onLoadSuccess: function () { //数据加载完毕事件
									$("#live_buildingObj_buildingId_edit").combobox("select", live.buildingObjPri);
									//var data = $("#live_buildingObj_buildingId_edit").combobox("getData"); 
						            //if (data.length > 0) {
						                //$("#live_buildingObj_buildingId_edit").combobox("select", data[0].buildingId);
						            //}
								}
							});
							$("#live_roomNo_edit").val(live.roomNo);
							$("#live_roomNo_edit").validatebox({
								required : true,
								missingMessage : "请输入入住宿舍号",
							});
							$("#live_studentObj_studentNo_edit").combobox({
								url:"Student/listAll",
							    valueField:"studentNo",
							    textField:"name",
							    panelHeight: "auto",
						        editable: false, //不允许手动输入 
						        onLoadSuccess: function () { //数据加载完毕事件
									$("#live_studentObj_studentNo_edit").combobox("select", live.studentObjPri);
									//var data = $("#live_studentObj_studentNo_edit").combobox("getData"); 
						            //if (data.length > 0) {
						                //$("#live_studentObj_studentNo_edit").combobox("select", data[0].studentNo);
						            //}
								}
							});
							$("#live_inDate_edit").datebox({
								value: live.inDate,
							    required: true,
							    showSeconds: true,
							});
							$("#live_liveMemo_edit").val(live.liveMemo);
						} else {
							$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
						}
					}
				});
			} else if (rows.length == 0) {
				$.messager.alert("警告操作！", "编辑记录至少选定一条数据！", "warning");
			}
		},
	};
}
