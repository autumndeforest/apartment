﻿$(function () {
	$("#live_buildingObj_buildingId").combobox({
	    url:'Building/listAll',
	    valueField: "buildingId",
	    textField: "buildingName",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#live_buildingObj_buildingId").combobox("getData"); 
            if (data.length > 0) {
                $("#live_buildingObj_buildingId").combobox("select", data[0].buildingId);
            }
        }
	});
	$("#live_roomNo").validatebox({
		required : true, 
		missingMessage : '请输入入住宿舍号',
	});

	$("#live_studentObj_studentNo").combobox({
	    url:'Student/listAll',
	    valueField: "studentNo",
	    textField: "name",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#live_studentObj_studentNo").combobox("getData"); 
            if (data.length > 0) {
                $("#live_studentObj_studentNo").combobox("select", data[0].studentNo);
            }
        }
	});
	$("#live_inDate").datebox({
	    required : true, 
	    showSeconds: true,
	    editable: false
	});

	//单击添加按钮
	$("#liveAddButton").click(function () {
		//验证表单 
		if(!$("#liveAddForm").form("validate")) {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		} else {
			$("#liveAddForm").form({
			    url:"Live/add",
			    onSubmit: function(){
					if($("#liveAddForm").form("validate"))  { 
	                	$.messager.progress({
							text : "正在提交数据中...",
						}); 
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                    //此处data={"Success":true}是字符串
                	var obj = jQuery.parseJSON(data); 
                    if(obj.success){ 
                        $.messager.alert("消息","保存成功！");
                        $(".messager-window").css("z-index",10000);
                        $("#liveAddForm").form("clear");
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    }
			    }
			});
			//提交表单
			$("#liveAddForm").submit();
		}
	});

	//单击清空按钮
	$("#liveClearButton").click(function () { 
		$("#liveAddForm").form("clear"); 
	});
});
