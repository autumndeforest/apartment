﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Room" %>
<%@ page import="com.org.po.Building" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    //获取所有的buildingObj信息
    List<Building> buildingList = (List<Building>)request.getAttribute("buildingList");
    Room room = (Room)request.getAttribute("room");

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
  <TITLE>修改宿舍信息</TITLE>
  <link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/animate.css" rel="stylesheet"> 
</head>
<body style="margin-top:70px;"> 
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="col-md-9 wow fadeInLeft">
	<ul class="breadcrumb">
  		<li><a href="<%=basePath %>index.jsp">首页</a></li>
  		<li class="active">宿舍信息修改</li>
	</ul>
		<div class="row"> 
      	<form class="form-horizontal" name="roomEditForm" id="roomEditForm" enctype="multipart/form-data" method="post"  class="mar_t15">
		  <div class="form-group">
			 <label for="room_roomId_edit" class="col-md-3 text-right">记录id:</label>
			 <div class="col-md-9"> 
			 	<input type="text" id="room_roomId_edit" name="room.roomId" class="form-control" placeholder="请输入记录id" readOnly>
			 </div>
		  </div> 
		  <div class="form-group">
		  	 <label for="room_buildingObj_buildingId_edit" class="col-md-3 text-right">所在公寓楼:</label>
		  	 <div class="col-md-9">
			    <select id="room_buildingObj_buildingId_edit" name="room.buildingObj.buildingId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="room_roomNo_edit" class="col-md-3 text-right">宿舍号:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="room_roomNo_edit" name="room.roomNo" class="form-control" placeholder="请输入宿舍号">
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="room_roomPhoto_edit" class="col-md-3 text-right">宿舍照片:</label>
		  	 <div class="col-md-9">
			    <img  class="img-responsive" id="room_roomPhotoImg" border="0px"/><br/>
			    <input type="hidden" id="room_roomPhoto" name="room.roomPhoto"/>
			    <input id="roomPhotoFile" name="roomPhotoFile" type="file" size="50" />
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="room_personNum_edit" class="col-md-3 text-right">床位数:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="room_personNum_edit" name="room.personNum" class="form-control" placeholder="请输入床位数">
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="room_roomDesc_edit" class="col-md-3 text-right">房间详情:</label>
		  	 <div class="col-md-9">
			    <script name="room.roomDesc" id="room_roomDesc_edit" type="text/plain"   style="width:100%;height:500px;"></script>
			 </div>
		  </div>
			  <div class="form-group">
			  	<span class="col-md-3""></span>
			  	<span onclick="ajaxRoomModify();" class="btn btn-primary bottom5 top5">修改</span>
			  </div>
		</form> 
	    <style>#roomEditForm .form-group {margin-bottom:5px;}  </style>
      </div>
   </div>
</div>


<jsp:include page="../footer.jsp"></jsp:include>
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jsdate.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/ueditor1_4_3/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/ueditor1_4_3/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/ueditor1_4_3/lang/zh-cn/zh-cn.js"></script>
<script>
var room_roomDesc_editor = UE.getEditor('room_roomDesc_edit'); //房间详情编辑框
var basePath = "<%=basePath%>";
/*弹出修改宿舍界面并初始化数据*/
function roomEdit(roomId) {
  room_roomDesc_editor.addListener("ready", function () {
    // editor准备好之后才可以使用 
    ajaxModifyQuery(roomId);
  });
}
 function ajaxModifyQuery(roomId) {
	$.ajax({
		url :  basePath + "Room/" + roomId + "/update",
		type : "get",
		dataType: "json",
		success : function (room, response, status) {
			if (room) {
				$("#room_roomId_edit").val(room.roomId);
				$.ajax({
					url: basePath + "Building/listAll",
					type: "get",
					success: function(buildings,response,status) { 
						$("#room_buildingObj_buildingId_edit").empty();
						var html="";
		        		$(buildings).each(function(i,building){
		        			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
		        		});
		        		$("#room_buildingObj_buildingId_edit").html(html);
		        		$("#room_buildingObj_buildingId_edit").val(room.buildingObjPri);
					}
				});
				$("#room_roomNo_edit").val(room.roomNo);
				$("#room_roomPhoto").val(room.roomPhoto);
				$("#room_roomPhotoImg").attr("src", basePath +　room.roomPhoto);
				$("#room_personNum_edit").val(room.personNum);
				room_roomDesc_editor.setContent(room.roomDesc, false);
			} else {
				alert("获取信息失败！");
			}
		}
	});
}

/*ajax方式提交宿舍信息表单给服务器端修改*/
function ajaxRoomModify() {
	$.ajax({
		url :  basePath + "Room/" + $("#room_roomId_edit").val() + "/update",
		type : "post",
		dataType: "json",
		data: new FormData($("#roomEditForm")[0]),
		success : function (obj, response, status) {
            if(obj.success){
                alert("信息修改成功！");
                location.reload(true);
                $("#roomQueryForm").submit();
            }else{
                alert(obj.message);
            } 
		},
		processData: false,
		contentType: false,
	});
}

$(function(){
        /*小屏幕导航点击关闭菜单*/
        $('.navbar-collapse a').click(function(){
            $('.navbar-collapse').collapse('hide');
        });
        new WOW().init();
    roomEdit("<%=request.getParameter("roomId")%>");
 })
 </script> 
</body>
</html>

