﻿$(function () {
	//实例化编辑器
	//建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
	UE.delEditor('room_roomDesc');
	var room_roomDesc_editor = UE.getEditor('room_roomDesc'); //房间详情编辑框
	$("#room_buildingObj_buildingId").combobox({
	    url:'Building/listAll',
	    valueField: "buildingId",
	    textField: "buildingName",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#room_buildingObj_buildingId").combobox("getData"); 
            if (data.length > 0) {
                $("#room_buildingObj_buildingId").combobox("select", data[0].buildingId);
            }
        }
	});
	$("#room_roomNo").validatebox({
		required : true, 
		missingMessage : '请输入宿舍号',
	});

	$("#room_personNum").validatebox({
		required : true,
		validType : "integer",
		missingMessage : '请输入床位数',
		invalidMessage : '床位数输入不对',
	});

	//单击添加按钮
	$("#roomAddButton").click(function () {
		if(room_roomDesc_editor.getContent() == "") {
			alert("请输入房间详情");
			return;
		}
		//验证表单 
		if(!$("#roomAddForm").form("validate")) {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		} else {
			$("#roomAddForm").form({
			    url:"Room/add",
			    onSubmit: function(){
					if($("#roomAddForm").form("validate"))  { 
	                	$.messager.progress({
							text : "正在提交数据中...",
						}); 
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                    //此处data={"Success":true}是字符串
                	var obj = jQuery.parseJSON(data); 
                    if(obj.success){ 
                        $.messager.alert("消息","保存成功！");
                        $(".messager-window").css("z-index",10000);
                        $("#roomAddForm").form("clear");
                        room_roomDesc_editor.setContent("");
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    }
			    }
			});
			//提交表单
			$("#roomAddForm").submit();
		}
	});

	//单击清空按钮
	$("#roomClearButton").click(function () { 
		$("#roomAddForm").form("clear"); 
	});
});
