﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/repairState.css" />
<div id="repairStateAddDiv">
	<form id="repairStateAddForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">状态名称:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repairState_stateName" name="repairState.stateName" style="width:200px" />

			</span>

		</div>
		<div class="operation">
			<a id="repairStateAddButton" class="easyui-linkbutton">添加</a>
			<a id="repairStateClearButton" class="easyui-linkbutton">重填</a>
		</div> 
	</form>
</div>
<script src="${pageContext.request.contextPath}/RepairState/js/repairState_add.js"></script> 
