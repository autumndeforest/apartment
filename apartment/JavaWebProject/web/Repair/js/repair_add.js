﻿$(function () {
	$("#repair_buildingObj_buildingId").combobox({
	    url:'Building/listAll',
	    valueField: "buildingId",
	    textField: "buildingName",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#repair_buildingObj_buildingId").combobox("getData"); 
            if (data.length > 0) {
                $("#repair_buildingObj_buildingId").combobox("select", data[0].buildingId);
            }
        }
	});
	$("#repair_roomNo").validatebox({
		required : true, 
		missingMessage : '请输入宿舍号',
	});

	$("#repair_repairItemObj_itemId").combobox({
	    url:'RepairItem/listAll',
	    valueField: "itemId",
	    textField: "itemName",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#repair_repairItemObj_itemId").combobox("getData"); 
            if (data.length > 0) {
                $("#repair_repairItemObj_itemId").combobox("select", data[0].itemId);
            }
        }
	});
	$("#repair_repairDesc").validatebox({
		required : true, 
		missingMessage : '请输入问题描述',
	});

	$("#repair_studentObj_studentNo").combobox({
	    url:'Student/listAll',
	    valueField: "studentNo",
	    textField: "name",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#repair_studentObj_studentNo").combobox("getData"); 
            if (data.length > 0) {
                $("#repair_studentObj_studentNo").combobox("select", data[0].studentNo);
            }
        }
	});
	$("#repair_addTime").datetimebox({
	    required : true, 
	    showSeconds: true,
	    editable: false
	});

	$("#repair_repairStateObj_stateId").combobox({
	    url:'RepairState/listAll',
	    valueField: "stateId",
	    textField: "stateName",
	    panelHeight: "auto",
        editable: false, //不允许手动输入
        required : true,
        onLoadSuccess: function () { //数据加载完毕事件
            var data = $("#repair_repairStateObj_stateId").combobox("getData"); 
            if (data.length > 0) {
                $("#repair_repairStateObj_stateId").combobox("select", data[0].stateId);
            }
        }
	});
	//单击添加按钮
	$("#repairAddButton").click(function () {
		//验证表单 
		if(!$("#repairAddForm").form("validate")) {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		} else {
			$("#repairAddForm").form({
			    url:"Repair/add",
			    onSubmit: function(){
					if($("#repairAddForm").form("validate"))  { 
	                	$.messager.progress({
							text : "正在提交数据中...",
						}); 
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                    //此处data={"Success":true}是字符串
                	var obj = jQuery.parseJSON(data); 
                    if(obj.success){ 
                        $.messager.alert("消息","保存成功！");
                        $(".messager-window").css("z-index",10000);
                        $("#repairAddForm").form("clear");
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    }
			    }
			});
			//提交表单
			$("#repairAddForm").submit();
		}
	});

	//单击清空按钮
	$("#repairClearButton").click(function () { 
		$("#repairAddForm").form("clear"); 
	});
});
