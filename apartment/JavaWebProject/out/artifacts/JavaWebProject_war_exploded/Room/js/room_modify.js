﻿$(function () {
	//实例化编辑器
	//建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
	UE.delEditor('room_roomDesc_edit');
	var room_roomDesc_edit = UE.getEditor('room_roomDesc_edit'); //房间详情编辑器
	room_roomDesc_edit.addListener("ready", function () {
		 // editor准备好之后才可以使用 
		 ajaxModifyQuery();
	}); 
  function ajaxModifyQuery() {	
	$.ajax({
		url : "Room/" + $("#room_roomId_edit").val() + "/update",
		type : "get",
		data : {
			//roomId : $("#room_roomId_edit").val(),
		},
		beforeSend : function () {
			$.messager.progress({
				text : "正在获取中...",
			});
		},
		success : function (room, response, status) {
			$.messager.progress("close");
			if (room) { 
				$("#room_roomId_edit").val(room.roomId);
				$("#room_roomId_edit").validatebox({
					required : true,
					missingMessage : "请输入记录id",
					editable: false
				});
				$("#room_buildingObj_buildingId_edit").combobox({
					url:"Building/listAll",
					valueField:"buildingId",
					textField:"buildingName",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#room_buildingObj_buildingId_edit").combobox("select", room.buildingObjPri);
						//var data = $("#room_buildingObj_buildingId_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#room_buildingObj_buildingId_edit").combobox("select", data[0].buildingId);
						//}
					}
				});
				$("#room_roomNo_edit").val(room.roomNo);
				$("#room_roomNo_edit").validatebox({
					required : true,
					missingMessage : "请输入宿舍号",
				});
				$("#room_roomPhoto").val(room.roomPhoto);
				$("#room_roomPhotoImg").attr("src", room.roomPhoto);
				$("#room_personNum_edit").val(room.personNum);
				$("#room_personNum_edit").validatebox({
					required : true,
					validType : "integer",
					missingMessage : "请输入床位数",
					invalidMessage : "床位数输入不对",
				});
				room_roomDesc_edit.setContent(room.roomDesc);
			} else {
				$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
				$(".messager-window").css("z-index",10000);
			}
		}
	});

  }

	$("#roomModifyButton").click(function(){ 
		if ($("#roomEditForm").form("validate")) {
			$("#roomEditForm").form({
			    url:"Room/" +  $("#room_roomId_edit").val() + "/update",
			    onSubmit: function(){
					if($("#roomEditForm").form("validate"))  {
	                	$.messager.progress({
							text : "正在提交数据中...",
						});
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                	var obj = jQuery.parseJSON(data);
                    if(obj.success){
                        $.messager.alert("消息","信息修改成功！");
                        $(".messager-window").css("z-index",10000);
                        //location.href="frontlist";
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    } 
			    }
			});
			//提交表单
			$("#roomEditForm").submit();
		} else {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		}
	});
});
