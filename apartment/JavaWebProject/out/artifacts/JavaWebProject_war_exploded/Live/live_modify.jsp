﻿<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/live.css" />
<div id="live_editDiv">
	<form id="liveEditForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">记录id:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_liveId_edit" name="live.liveId" value="<%=request.getParameter("liveId") %>" style="width:200px" />
			</span>
		</div>

		<div>
			<span class="label">入住公寓楼:</span>
			<span class="inputControl">
				<input class="textbox"  id="live_buildingObj_buildingId_edit" name="live.buildingObj.buildingId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">入住宿舍号:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_roomNo_edit" name="live.roomNo" style="width:200px" />

			</span>

		</div>
		<div>
			<span class="label">入住学生:</span>
			<span class="inputControl">
				<input class="textbox"  id="live_studentObj_studentNo_edit" name="live.studentObj.studentNo" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">入住日期:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_inDate_edit" name="live.inDate" />

			</span>

		</div>
		<div>
			<span class="label">备注信息:</span>
			<span class="inputControl">
				<textarea id="live_liveMemo_edit" name="live.liveMemo" rows="8" cols="60"></textarea>

			</span>

		</div>
		<div class="operation">
			<a id="liveModifyButton" class="easyui-linkbutton">更新</a> 
		</div>
	</form>
</div>
<script src="${pageContext.request.contextPath}/Live/js/live_modify.js"></script> 
