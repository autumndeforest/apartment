﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Live" %>
<%@ page import="com.org.po.Building" %>
<%@ page import="com.org.po.Student" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    List<Live> liveList = (List<Live>)request.getAttribute("liveList");
    //获取所有的buildingObj信息
    List<Building> buildingList = (List<Building>)request.getAttribute("buildingList");
    //获取所有的studentObj信息
    List<Student> studentList = (List<Student>)request.getAttribute("studentList");
    int currentPage =  (Integer)request.getAttribute("currentPage"); //当前页
    int totalPage =   (Integer)request.getAttribute("totalPage");  //一共多少页
    int recordNumber =   (Integer)request.getAttribute("recordNumber");  //一共多少记录
    Building buildingObj = (Building)request.getAttribute("buildingObj");
    String roomNo = (String)request.getAttribute("roomNo"); //入住宿舍号查询关键字
    Student studentObj = (Student)request.getAttribute("studentObj");
    String inDate = (String)request.getAttribute("inDate"); //入住日期查询关键字
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
<title>住宿查询</title>
<link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
<link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
<link href="<%=basePath %>plugins/animate.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body style="margin-top:70px;">
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="row"> 
		<div class="col-md-9 wow fadeInDown" data-wow-duration="0.5s">
			<div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
			    	<li><a href="<%=basePath %>index.jsp">首页</a></li>
			    	<li role="presentation" class="active"><a href="#liveListPanel" aria-controls="liveListPanel" role="tab" data-toggle="tab">住宿列表</a></li>
			    	<li role="presentation" ><a href="<%=basePath %>Live/live_frontAdd.jsp" style="display:none;">添加住宿</a></li>
				</ul>
			  	<!-- Tab panes -->
			  	<div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="liveListPanel">
				    		<div class="row">
				    			<div class="col-md-12 top5">
				    				<div class="table-responsive">
				    				<table class="table table-condensed table-hover">
				    					<tr class="success bold"><td>序号</td><td>记录id</td><td>入住公寓楼</td><td>入住宿舍号</td><td>入住学生</td><td>入住日期</td><td>操作</td></tr>
				    					<% 
				    						/*计算起始序号*/
				    	            		int startIndex = (currentPage -1) * 5;
				    	            		/*遍历记录*/
				    	            		for(int i=0;i<liveList.size();i++) {
					    	            		int currentIndex = startIndex + i + 1; //当前记录的序号
					    	            		Live live = liveList.get(i); //获取到住宿对象
 										%>
 										<tr>
 											<td><%=currentIndex %></td>
 											<td><%=live.getLiveId() %></td>
 											<td><%=live.getBuildingObj().getBuildingName() %></td>
 											<td><%=live.getRoomNo() %></td>
 											<td><%=live.getStudentObj().getName() %></td>
 											<td><%=live.getInDate() %></td>
 											<td>
 												<a href="<%=basePath  %>Live/<%=live.getLiveId() %>/frontshow"><i class="fa fa-info"></i>&nbsp;查看</a>&nbsp;
 												<a href="#" onclick="liveEdit('<%=live.getLiveId() %>');" style="display:none;"><i class="fa fa-pencil fa-fw"></i>编辑</a>&nbsp;
 												<a href="#" onclick="liveDelete('<%=live.getLiveId() %>');" style="display:none;"><i class="fa fa-trash-o fa-fw"></i>删除</a>
 											</td> 
 										</tr>
 										<%}%>
				    				</table>
				    				</div>
				    			</div>
				    		</div>

				    		<div class="row">
					            <div class="col-md-12">
						            <nav class="pull-left">
						                <ul class="pagination">
						                    <li><a href="#" onclick="GoToPage(<%=currentPage-1 %>,<%=totalPage %>);" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
						                     <%
						                    	int startPage = currentPage - 5;
						                    	int endPage = currentPage + 5;
						                    	if(startPage < 1) startPage=1;
						                    	if(endPage > totalPage) endPage = totalPage;
						                    	for(int i=startPage;i<=endPage;i++) {
						                    %>
						                    <li class="<%= currentPage==i?"active":"" %>"><a href="#"  onclick="GoToPage(<%=i %>,<%=totalPage %>);"><%=i %></a></li>
						                    <%  } %> 
						                    <li><a href="#" onclick="GoToPage(<%=currentPage+1 %>,<%=totalPage %>);"><span aria-hidden="true">&raquo;</span></a></li>
						                </ul>
						            </nav>
						            <div class="pull-right" style="line-height:75px;" >共有<%=recordNumber %>条记录，当前第 <%=currentPage %>/<%=totalPage %> 页</div>
					            </div>
				            </div> 
				    </div>
				</div>
			</div>
		</div>
	<div class="col-md-3 wow fadeInRight">
		<div class="page-header">
    		<h1>住宿查询</h1>
		</div>
		<form name="liveQueryForm" id="liveQueryForm" action="<%=basePath %>Live/frontlist" class="mar_t15">
            <div class="form-group">
            	<label for="buildingObj_buildingId">入住公寓楼：</label>
                <select id="buildingObj_buildingId" name="buildingObj.buildingId" class="form-control">
                	<option value="0">不限制</option>
	 				<%
	 				for(Building buildingTemp:buildingList) {
	 					String selected = "";
 					if(buildingObj!=null && buildingObj.getBuildingId()!=null && buildingObj.getBuildingId().intValue()==buildingTemp.getBuildingId().intValue())
 						selected = "selected";
	 				%>
 				 <option value="<%=buildingTemp.getBuildingId() %>" <%=selected %>><%=buildingTemp.getBuildingName() %></option>
	 				<%
	 				}
	 				%>
 			</select>
            </div>
			<div class="form-group">
				<label for="roomNo">入住宿舍号:</label>
				<input type="text" id="roomNo" name="roomNo" value="<%=roomNo %>" class="form-control" placeholder="请输入入住宿舍号">
			</div>






            <div class="form-group">
            	<label for="studentObj_studentNo">入住学生：</label>
                <select id="studentObj_studentNo" name="studentObj.studentNo" class="form-control">
                	<option value="">不限制</option>
	 				<%
	 				for(Student studentTemp:studentList) {
	 					String selected = "";
 					if(studentObj!=null && studentObj.getStudentNo()!=null && studentObj.getStudentNo().equals(studentTemp.getStudentNo()))
 						selected = "selected";
	 				%>
 				 <option value="<%=studentTemp.getStudentNo() %>" <%=selected %>><%=studentTemp.getName() %></option>
	 				<%
	 				}
	 				%>
 			</select>
            </div>
			<div class="form-group">
				<label for="inDate">入住日期:</label>
				<input type="text" id="inDate" name="inDate" class="form-control"  placeholder="请选择入住日期" value="<%=inDate %>" onclick="SelectDate(this,'yyyy-MM-dd')" />
			</div>
            <input type=hidden name=currentPage value="<%=currentPage %>" />
            <button type="submit" class="btn btn-primary">查询</button>
        </form>
	</div>

		</div>
	</div> 
<div id="liveEditDialog" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;住宿信息编辑</h4>
      </div>
      <div class="modal-body" style="height:450px; overflow: scroll;">
      	<form class="form-horizontal" name="liveEditForm" id="liveEditForm" enctype="multipart/form-data" method="post"  class="mar_t15">
		  <div class="form-group">
			 <label for="live_liveId_edit" class="col-md-3 text-right">记录id:</label>
			 <div class="col-md-9"> 
			 	<input type="text" id="live_liveId_edit" name="live.liveId" class="form-control" placeholder="请输入记录id" readOnly>
			 </div>
		  </div> 
		  <div class="form-group">
		  	 <label for="live_buildingObj_buildingId_edit" class="col-md-3 text-right">入住公寓楼:</label>
		  	 <div class="col-md-9">
			    <select id="live_buildingObj_buildingId_edit" name="live.buildingObj.buildingId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_roomNo_edit" class="col-md-3 text-right">入住宿舍号:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="live_roomNo_edit" name="live.roomNo" class="form-control" placeholder="请输入入住宿舍号">
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_studentObj_studentNo_edit" class="col-md-3 text-right">入住学生:</label>
		  	 <div class="col-md-9">
			    <select id="live_studentObj_studentNo_edit" name="live.studentObj.studentNo" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_inDate_edit" class="col-md-3 text-right">入住日期:</label>
		  	 <div class="col-md-9">
                <div class="input-group date live_inDate_edit col-md-12" data-link-field="live_inDate_edit"  data-link-format="yyyy-mm-dd">
                    <input class="form-control" id="live_inDate_edit" name="live.inDate" size="16" type="text" value="" placeholder="请选择入住日期" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_liveMemo_edit" class="col-md-3 text-right">备注信息:</label>
		  	 <div class="col-md-9">
			    <textarea id="live_liveMemo_edit" name="live.liveMemo" rows="8" class="form-control" placeholder="请输入备注信息"></textarea>
			 </div>
		  </div>
		</form> 
	    <style>#liveEditForm .form-group {margin-bottom:5px;}  </style>
      </div>
      <div class="modal-footer"> 
      	<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
      	<button type="button" class="btn btn-primary" onclick="ajaxLiveModify();">提交</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<jsp:include page="../footer.jsp"></jsp:include> 
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jsdate.js"></script>
<script>
var basePath = "<%=basePath%>";
/*跳转到查询结果的某页*/
function GoToPage(currentPage,totalPage) {
    if(currentPage==0) return;
    if(currentPage>totalPage) return;
    document.liveQueryForm.currentPage.value = currentPage;
    document.liveQueryForm.submit();
}

/*可以直接跳转到某页*/
function changepage(totalPage)
{
    var pageValue=document.liveQueryForm.pageValue.value;
    if(pageValue>totalPage) {
        alert('你输入的页码超出了总页数!');
        return ;
    }
    document.liveQueryForm.currentPage.value = pageValue;
    documentliveQueryForm.submit();
}

/*弹出修改住宿界面并初始化数据*/
function liveEdit(liveId) {
	$.ajax({
		url :  basePath + "Live/" + liveId + "/update",
		type : "get",
		dataType: "json",
		success : function (live, response, status) {
			if (live) {
				$("#live_liveId_edit").val(live.liveId);
				$.ajax({
					url: basePath + "Building/listAll",
					type: "get",
					success: function(buildings,response,status) { 
						$("#live_buildingObj_buildingId_edit").empty();
						var html="";
		        		$(buildings).each(function(i,building){
		        			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
		        		});
		        		$("#live_buildingObj_buildingId_edit").html(html);
		        		$("#live_buildingObj_buildingId_edit").val(live.buildingObjPri);
					}
				});
				$("#live_roomNo_edit").val(live.roomNo);
				$.ajax({
					url: basePath + "Student/listAll",
					type: "get",
					success: function(students,response,status) { 
						$("#live_studentObj_studentNo_edit").empty();
						var html="";
		        		$(students).each(function(i,student){
		        			html += "<option value='" + student.studentNo + "'>" + student.name + "</option>";
		        		});
		        		$("#live_studentObj_studentNo_edit").html(html);
		        		$("#live_studentObj_studentNo_edit").val(live.studentObjPri);
					}
				});
				$("#live_inDate_edit").val(live.inDate);
				$("#live_liveMemo_edit").val(live.liveMemo);
				$('#liveEditDialog').modal('show');
			} else {
				alert("获取信息失败！");
			}
		}
	});
}

/*删除住宿信息*/
function liveDelete(liveId) {
	if(confirm("确认删除这个记录")) {
		$.ajax({
			type : "POST",
			url : basePath + "Live/deletes",
			data : {
				liveIds : liveId,
			},
			success : function (obj) {
				if (obj.success) {
					alert("删除成功");
					$("#liveQueryForm").submit();
					//location.href= basePath + "Live/frontlist";
				}
				else 
					alert(obj.message);
			},
		});
	}
}

/*ajax方式提交住宿信息表单给服务器端修改*/
function ajaxLiveModify() {
	$.ajax({
		url :  basePath + "Live/" + $("#live_liveId_edit").val() + "/update",
		type : "post",
		dataType: "json",
		data: new FormData($("#liveEditForm")[0]),
		success : function (obj, response, status) {
            if(obj.success){
                alert("信息修改成功！");
                $("#liveQueryForm").submit();
            }else{
                alert(obj.message);
            } 
		},
		processData: false,
		contentType: false,
	});
}

$(function(){
	/*小屏幕导航点击关闭菜单*/
    $('.navbar-collapse a').click(function(){
        $('.navbar-collapse').collapse('hide');
    });
    new WOW().init();

    /*入住日期组件*/
    $('.live_inDate_edit').datetimepicker({
    	language:  'zh-CN',  //语言
    	format: 'yyyy-mm-dd',
    	minView: 2,
    	weekStart: 1,
    	todayBtn:  1,
    	autoclose: 1,
    	minuteStep: 1,
    	todayHighlight: 1,
    	startView: 2,
    	forceParse: 0
    });
})
</script>
</body>
</html>

