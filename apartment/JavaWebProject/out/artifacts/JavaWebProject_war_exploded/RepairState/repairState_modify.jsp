﻿<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/repairState.css" />
<div id="repairState_editDiv">
	<form id="repairStateEditForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">状态id:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repairState_stateId_edit" name="repairState.stateId" value="<%=request.getParameter("stateId") %>" style="width:200px" />
			</span>
		</div>

		<div>
			<span class="label">状态名称:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repairState_stateName_edit" name="repairState.stateName" style="width:200px" />

			</span>

		</div>
		<div class="operation">
			<a id="repairStateModifyButton" class="easyui-linkbutton">更新</a> 
		</div>
	</form>
</div>
<script src="${pageContext.request.contextPath}/RepairState/js/repairState_modify.js"></script> 
