﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Repair" %>
<%@ page import="com.org.po.Building" %>
<%@ page import="com.org.po.RepairItem" %>
<%@ page import="com.org.po.RepairState" %>
<%@ page import="com.org.po.Student" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    //获取所有的buildingObj信息
    List<Building> buildingList = (List<Building>)request.getAttribute("buildingList");
    //获取所有的repairItemObj信息
    List<RepairItem> repairItemList = (List<RepairItem>)request.getAttribute("repairItemList");
    //获取所有的repairStateObj信息
    List<RepairState> repairStateList = (List<RepairState>)request.getAttribute("repairStateList");
    //获取所有的studentObj信息
    List<Student> studentList = (List<Student>)request.getAttribute("studentList");
    Repair repair = (Repair)request.getAttribute("repair");

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
  <TITLE>修改报修信息</TITLE>
  <link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/animate.css" rel="stylesheet"> 
</head>
<body style="margin-top:70px;"> 
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="col-md-9 wow fadeInLeft">
	<ul class="breadcrumb">
  		<li><a href="<%=basePath %>index.jsp">首页</a></li>
  		<li class="active">报修信息修改</li>
	</ul>
		<div class="row"> 
      	<form class="form-horizontal" name="repairEditForm" id="repairEditForm" enctype="multipart/form-data" method="post"  class="mar_t15">
		  <div class="form-group">
			 <label for="repair_repairId_edit" class="col-md-3 text-right">报修id:</label>
			 <div class="col-md-9"> 
			 	<input type="text" id="repair_repairId_edit" name="repair.repairId" class="form-control" placeholder="请输入报修id" readOnly>
			 </div>
		  </div> 
		  <div class="form-group">
		  	 <label for="repair_buildingObj_buildingId_edit" class="col-md-3 text-right">公寓楼:</label>
		  	 <div class="col-md-9">
			    <select id="repair_buildingObj_buildingId_edit" name="repair.buildingObj.buildingId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_roomNo_edit" class="col-md-3 text-right">宿舍号:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="repair_roomNo_edit" name="repair.roomNo" class="form-control" placeholder="请输入宿舍号">
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_repairItemObj_itemId_edit" class="col-md-3 text-right">报修项目:</label>
		  	 <div class="col-md-9">
			    <select id="repair_repairItemObj_itemId_edit" name="repair.repairItemObj.itemId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_repairDesc_edit" class="col-md-3 text-right">问题描述:</label>
		  	 <div class="col-md-9">
			    <textarea id="repair_repairDesc_edit" name="repair.repairDesc" rows="8" class="form-control" placeholder="请输入问题描述"></textarea>
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_studentObj_studentNo_edit" class="col-md-3 text-right">上报学生:</label>
		  	 <div class="col-md-9">
			    <select id="repair_studentObj_studentNo_edit" name="repair.studentObj.studentNo" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_addTime_edit" class="col-md-3 text-right">上报时间:</label>
		  	 <div class="col-md-9">
                <div class="input-group date repair_addTime_edit col-md-12" data-link-field="repair_addTime_edit">
                    <input class="form-control" id="repair_addTime_edit" name="repair.addTime" size="16" type="text" value="" placeholder="请选择上报时间" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_repairStateObj_stateId_edit" class="col-md-3 text-right">维修状态:</label>
		  	 <div class="col-md-9">
			    <select id="repair_repairStateObj_stateId_edit" name="repair.repairStateObj.stateId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_handleResult_edit" class="col-md-3 text-right">处理结果:</label>
		  	 <div class="col-md-9">
			    <textarea id="repair_handleResult_edit" name="repair.handleResult" rows="8" class="form-control" placeholder="请输入处理结果"></textarea>
			 </div>
		  </div>
			  <div class="form-group">
			  	<span class="col-md-3""></span>
			  	<span onclick="ajaxRepairModify();" class="btn btn-primary bottom5 top5">修改</span>
			  </div>
		</form> 
	    <style>#repairEditForm .form-group {margin-bottom:5px;}  </style>
      </div>
   </div>
</div>


<jsp:include page="../footer.jsp"></jsp:include>
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jsdate.js"></script>
<script>
var basePath = "<%=basePath%>";
/*弹出修改报修界面并初始化数据*/
function repairEdit(repairId) {
	$.ajax({
		url :  basePath + "Repair/" + repairId + "/update",
		type : "get",
		dataType: "json",
		success : function (repair, response, status) {
			if (repair) {
				$("#repair_repairId_edit").val(repair.repairId);
				$.ajax({
					url: basePath + "Building/listAll",
					type: "get",
					success: function(buildings,response,status) { 
						$("#repair_buildingObj_buildingId_edit").empty();
						var html="";
		        		$(buildings).each(function(i,building){
		        			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
		        		});
		        		$("#repair_buildingObj_buildingId_edit").html(html);
		        		$("#repair_buildingObj_buildingId_edit").val(repair.buildingObjPri);
					}
				});
				$("#repair_roomNo_edit").val(repair.roomNo);
				$.ajax({
					url: basePath + "RepairItem/listAll",
					type: "get",
					success: function(repairItems,response,status) { 
						$("#repair_repairItemObj_itemId_edit").empty();
						var html="";
		        		$(repairItems).each(function(i,repairItem){
		        			html += "<option value='" + repairItem.itemId + "'>" + repairItem.itemName + "</option>";
		        		});
		        		$("#repair_repairItemObj_itemId_edit").html(html);
		        		$("#repair_repairItemObj_itemId_edit").val(repair.repairItemObjPri);
					}
				});
				$("#repair_repairDesc_edit").val(repair.repairDesc);
				$.ajax({
					url: basePath + "Student/listAll",
					type: "get",
					success: function(students,response,status) { 
						$("#repair_studentObj_studentNo_edit").empty();
						var html="";
		        		$(students).each(function(i,student){
		        			html += "<option value='" + student.studentNo + "'>" + student.name + "</option>";
		        		});
		        		$("#repair_studentObj_studentNo_edit").html(html);
		        		$("#repair_studentObj_studentNo_edit").val(repair.studentObjPri);
					}
				});
				$("#repair_addTime_edit").val(repair.addTime);
				$.ajax({
					url: basePath + "RepairState/listAll",
					type: "get",
					success: function(repairStates,response,status) { 
						$("#repair_repairStateObj_stateId_edit").empty();
						var html="";
		        		$(repairStates).each(function(i,repairState){
		        			html += "<option value='" + repairState.stateId + "'>" + repairState.stateName + "</option>";
		        		});
		        		$("#repair_repairStateObj_stateId_edit").html(html);
		        		$("#repair_repairStateObj_stateId_edit").val(repair.repairStateObjPri);
					}
				});
				$("#repair_handleResult_edit").val(repair.handleResult);
			} else {
				alert("获取信息失败！");
			}
		}
	});
}

/*ajax方式提交报修信息表单给服务器端修改*/
function ajaxRepairModify() {
	$.ajax({
		url :  basePath + "Repair/" + $("#repair_repairId_edit").val() + "/update",
		type : "post",
		dataType: "json",
		data: new FormData($("#repairEditForm")[0]),
		success : function (obj, response, status) {
            if(obj.success){
                alert("信息修改成功！");
                location.reload(true);
                $("#repairQueryForm").submit();
            }else{
                alert(obj.message);
            } 
		},
		processData: false,
		contentType: false,
	});
}

$(function(){
        /*小屏幕导航点击关闭菜单*/
        $('.navbar-collapse a').click(function(){
            $('.navbar-collapse').collapse('hide');
        });
        new WOW().init();
    /*上报时间组件*/
    $('.repair_addTime_edit').datetimepicker({
    	language:  'zh-CN',  //语言
    	format: 'yyyy-mm-dd hh:ii:ss',
    	weekStart: 1,
    	todayBtn:  1,
    	autoclose: 1,
    	minuteStep: 1,
    	todayHighlight: 1,
    	startView: 2,
    	forceParse: 0
    });
    repairEdit("<%=request.getParameter("repairId")%>");
 })
 </script> 
</body>
</html>

