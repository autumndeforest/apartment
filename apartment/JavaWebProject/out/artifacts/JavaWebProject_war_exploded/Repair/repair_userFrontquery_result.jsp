﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Repair" %>
<%@ page import="com.org.po.Building" %>
<%@ page import="com.org.po.RepairItem" %>
<%@ page import="com.org.po.RepairState" %>
<%@ page import="com.org.po.Student" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    List<Repair> repairList = (List<Repair>)request.getAttribute("repairList");
    //获取所有的buildingObj信息
    List<Building> buildingList = (List<Building>)request.getAttribute("buildingList");
    //获取所有的repairItemObj信息
    List<RepairItem> repairItemList = (List<RepairItem>)request.getAttribute("repairItemList");
    //获取所有的repairStateObj信息
    List<RepairState> repairStateList = (List<RepairState>)request.getAttribute("repairStateList");
    //获取所有的studentObj信息
    List<Student> studentList = (List<Student>)request.getAttribute("studentList");
    int currentPage =  (Integer)request.getAttribute("currentPage"); //当前页
    int totalPage =   (Integer)request.getAttribute("totalPage");  //一共多少页
    int recordNumber =   (Integer)request.getAttribute("recordNumber");  //一共多少记录
    Building buildingObj = (Building)request.getAttribute("buildingObj");
    String roomNo = (String)request.getAttribute("roomNo"); //宿舍号查询关键字
    RepairItem repairItemObj = (RepairItem)request.getAttribute("repairItemObj");
    Student studentObj = (Student)request.getAttribute("studentObj");
    String addTime = (String)request.getAttribute("addTime"); //上报时间查询关键字
    RepairState repairStateObj = (RepairState)request.getAttribute("repairStateObj");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
<title>报修查询</title>
<link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
<link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
<link href="<%=basePath %>plugins/animate.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body style="margin-top:70px;">
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="row"> 
		<div class="col-md-9 wow fadeInDown" data-wow-duration="0.5s">
			<div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
			    	<li><a href="<%=basePath %>index.jsp">首页</a></li>
			    	<li role="presentation" class="active"><a href="#repairListPanel" aria-controls="repairListPanel" role="tab" data-toggle="tab">报修列表</a></li>
			    	<li role="presentation" ><a href="<%=basePath %>Repair/repair_frontAdd.jsp" style="display:none;">添加报修</a></li>
				</ul>
			  	<!-- Tab panes -->
			  	<div class="tab-content">
				    <div role="tabpanel" class="tab-pane active" id="repairListPanel">
				    		<div class="row">
				    			<div class="col-md-12 top5">
				    				<div class="table-responsive">
				    				<table class="table table-condensed table-hover">
				    					<tr class="success bold"><td>序号</td><td>报修id</td><td>公寓楼</td><td>宿舍号</td><td>报修项目</td><td>上报时间</td><td>维修状态</td><td>操作</td></tr>
				    					<% 
				    						/*计算起始序号*/
				    	            		int startIndex = (currentPage -1) * 5;
				    	            		/*遍历记录*/
				    	            		for(int i=0;i<repairList.size();i++) {
					    	            		int currentIndex = startIndex + i + 1; //当前记录的序号
					    	            		Repair repair = repairList.get(i); //获取到报修对象
 										%>
 										<tr>
 											<td><%=currentIndex %></td>
 											<td><%=repair.getRepairId() %></td>
 											<td><%=repair.getBuildingObj().getBuildingName() %></td>
 											<td><%=repair.getRoomNo() %></td>
 											<td><%=repair.getRepairItemObj().getItemName() %></td>
 											<td><%=repair.getAddTime() %></td>
 											<td><%=repair.getRepairStateObj().getStateName() %></td>
 											<td>
 												<a href="<%=basePath  %>Repair/<%=repair.getRepairId() %>/frontshow"><i class="fa fa-info"></i>&nbsp;查看</a>&nbsp;
 												<a href="#" onclick="repairEdit('<%=repair.getRepairId() %>');" style="display:none;"><i class="fa fa-pencil fa-fw"></i>编辑</a>&nbsp;
 												<a href="#" onclick="repairDelete('<%=repair.getRepairId() %>');"><i class="fa fa-trash-o fa-fw"></i>删除</a>
 											</td> 
 										</tr>
 										<%}%>
				    				</table>
				    				</div>
				    			</div>
				    		</div>

				    		<div class="row">
					            <div class="col-md-12">
						            <nav class="pull-left">
						                <ul class="pagination">
						                    <li><a href="#" onclick="GoToPage(<%=currentPage-1 %>,<%=totalPage %>);" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
						                     <%
						                    	int startPage = currentPage - 5;
						                    	int endPage = currentPage + 5;
						                    	if(startPage < 1) startPage=1;
						                    	if(endPage > totalPage) endPage = totalPage;
						                    	for(int i=startPage;i<=endPage;i++) {
						                    %>
						                    <li class="<%= currentPage==i?"active":"" %>"><a href="#"  onclick="GoToPage(<%=i %>,<%=totalPage %>);"><%=i %></a></li>
						                    <%  } %> 
						                    <li><a href="#" onclick="GoToPage(<%=currentPage+1 %>,<%=totalPage %>);"><span aria-hidden="true">&raquo;</span></a></li>
						                </ul>
						            </nav>
						            <div class="pull-right" style="line-height:75px;" >共有<%=recordNumber %>条记录，当前第 <%=currentPage %>/<%=totalPage %> 页</div>
					            </div>
				            </div> 
				    </div>
				</div>
			</div>
		</div>
	<div class="col-md-3 wow fadeInRight">
		<div class="page-header">
    		<h1>报修查询</h1>
		</div>
		<form name="repairQueryForm" id="repairQueryForm" action="<%=basePath %>Repair/userFrontlist" class="mar_t15">
            <div class="form-group">
            	<label for="buildingObj_buildingId">公寓楼：</label>
                <select id="buildingObj_buildingId" name="buildingObj.buildingId" class="form-control">
                	<option value="0">不限制</option>
	 				<%
	 				for(Building buildingTemp:buildingList) {
	 					String selected = "";
 					if(buildingObj!=null && buildingObj.getBuildingId()!=null && buildingObj.getBuildingId().intValue()==buildingTemp.getBuildingId().intValue())
 						selected = "selected";
	 				%>
 				 <option value="<%=buildingTemp.getBuildingId() %>" <%=selected %>><%=buildingTemp.getBuildingName() %></option>
	 				<%
	 				}
	 				%>
 			</select>
            </div>
			<div class="form-group">
				<label for="roomNo">宿舍号:</label>
				<input type="text" id="roomNo" name="roomNo" value="<%=roomNo %>" class="form-control" placeholder="请输入宿舍号">
			</div>






            <div class="form-group">
            	<label for="repairItemObj_itemId">报修项目：</label>
                <select id="repairItemObj_itemId" name="repairItemObj.itemId" class="form-control">
                	<option value="0">不限制</option>
	 				<%
	 				for(RepairItem repairItemTemp:repairItemList) {
	 					String selected = "";
 					if(repairItemObj!=null && repairItemObj.getItemId()!=null && repairItemObj.getItemId().intValue()==repairItemTemp.getItemId().intValue())
 						selected = "selected";
	 				%>
 				 <option value="<%=repairItemTemp.getItemId() %>" <%=selected %>><%=repairItemTemp.getItemName() %></option>
	 				<%
	 				}
	 				%>
 			</select>
            </div>
            <div class="form-group" style="display:none;">
            	<label for="studentObj_studentNo">上报学生：</label>
                <select id="studentObj_studentNo" name="studentObj.studentNo" class="form-control">
                	<option value="">不限制</option>
	 				<%
	 				for(Student studentTemp:studentList) {
	 					String selected = "";
 					if(studentObj!=null && studentObj.getStudentNo()!=null && studentObj.getStudentNo().equals(studentTemp.getStudentNo()))
 						selected = "selected";
	 				%>
 				 <option value="<%=studentTemp.getStudentNo() %>" <%=selected %>><%=studentTemp.getName() %></option>
	 				<%
	 				}
	 				%>
 			</select>
            </div>
			<div class="form-group">
				<label for="addTime">上报时间:</label>
				<input type="text" id="addTime" name="addTime" class="form-control"  placeholder="请选择上报时间" value="<%=addTime %>" onclick="SelectDate(this,'yyyy-MM-dd')" />
			</div>
            <div class="form-group">
            	<label for="repairStateObj_stateId">维修状态：</label>
                <select id="repairStateObj_stateId" name="repairStateObj.stateId" class="form-control">
                	<option value="0">不限制</option>
	 				<%
	 				for(RepairState repairStateTemp:repairStateList) {
	 					String selected = "";
 					if(repairStateObj!=null && repairStateObj.getStateId()!=null && repairStateObj.getStateId().intValue()==repairStateTemp.getStateId().intValue())
 						selected = "selected";
	 				%>
 				 <option value="<%=repairStateTemp.getStateId() %>" <%=selected %>><%=repairStateTemp.getStateName() %></option>
	 				<%
	 				}
	 				%>
 			</select>
            </div>
            <input type=hidden name=currentPage value="<%=currentPage %>" />
            <button type="submit" class="btn btn-primary">查询</button>
        </form>
	</div>

		</div>
	</div> 
<div id="repairEditDialog" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-edit"></i>&nbsp;报修信息编辑</h4>
      </div>
      <div class="modal-body" style="height:450px; overflow: scroll;">
      	<form class="form-horizontal" name="repairEditForm" id="repairEditForm" enctype="multipart/form-data" method="post"  class="mar_t15">
		  <div class="form-group">
			 <label for="repair_repairId_edit" class="col-md-3 text-right">报修id:</label>
			 <div class="col-md-9"> 
			 	<input type="text" id="repair_repairId_edit" name="repair.repairId" class="form-control" placeholder="请输入报修id" readOnly>
			 </div>
		  </div> 
		  <div class="form-group">
		  	 <label for="repair_buildingObj_buildingId_edit" class="col-md-3 text-right">公寓楼:</label>
		  	 <div class="col-md-9">
			    <select id="repair_buildingObj_buildingId_edit" name="repair.buildingObj.buildingId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_roomNo_edit" class="col-md-3 text-right">宿舍号:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="repair_roomNo_edit" name="repair.roomNo" class="form-control" placeholder="请输入宿舍号">
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_repairItemObj_itemId_edit" class="col-md-3 text-right">报修项目:</label>
		  	 <div class="col-md-9">
			    <select id="repair_repairItemObj_itemId_edit" name="repair.repairItemObj.itemId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_repairDesc_edit" class="col-md-3 text-right">问题描述:</label>
		  	 <div class="col-md-9">
			    <textarea id="repair_repairDesc_edit" name="repair.repairDesc" rows="8" class="form-control" placeholder="请输入问题描述"></textarea>
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_studentObj_studentNo_edit" class="col-md-3 text-right">上报学生:</label>
		  	 <div class="col-md-9">
			    <select id="repair_studentObj_studentNo_edit" name="repair.studentObj.studentNo" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_addTime_edit" class="col-md-3 text-right">上报时间:</label>
		  	 <div class="col-md-9">
                <div class="input-group date repair_addTime_edit col-md-12" data-link-field="repair_addTime_edit">
                    <input class="form-control" id="repair_addTime_edit" name="repair.addTime" size="16" type="text" value="" placeholder="请选择上报时间" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_repairStateObj_stateId_edit" class="col-md-3 text-right">维修状态:</label>
		  	 <div class="col-md-9">
			    <select id="repair_repairStateObj_stateId_edit" name="repair.repairStateObj.stateId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="repair_handleResult_edit" class="col-md-3 text-right">处理结果:</label>
		  	 <div class="col-md-9">
			    <textarea id="repair_handleResult_edit" name="repair.handleResult" rows="8" class="form-control" placeholder="请输入处理结果"></textarea>
			 </div>
		  </div>
		</form> 
	    <style>#repairEditForm .form-group {margin-bottom:5px;}  </style>
      </div>
      <div class="modal-footer"> 
      	<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
      	<button type="button" class="btn btn-primary" onclick="ajaxRepairModify();">提交</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<jsp:include page="../footer.jsp"></jsp:include> 
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jsdate.js"></script>
<script>
var basePath = "<%=basePath%>";
/*跳转到查询结果的某页*/
function GoToPage(currentPage,totalPage) {
    if(currentPage==0) return;
    if(currentPage>totalPage) return;
    document.repairQueryForm.currentPage.value = currentPage;
    document.repairQueryForm.submit();
}

/*可以直接跳转到某页*/
function changepage(totalPage)
{
    var pageValue=document.repairQueryForm.pageValue.value;
    if(pageValue>totalPage) {
        alert('你输入的页码超出了总页数!');
        return ;
    }
    document.repairQueryForm.currentPage.value = pageValue;
    documentrepairQueryForm.submit();
}

/*弹出修改报修界面并初始化数据*/
function repairEdit(repairId) {
	$.ajax({
		url :  basePath + "Repair/" + repairId + "/update",
		type : "get",
		dataType: "json",
		success : function (repair, response, status) {
			if (repair) {
				$("#repair_repairId_edit").val(repair.repairId);
				$.ajax({
					url: basePath + "Building/listAll",
					type: "get",
					success: function(buildings,response,status) { 
						$("#repair_buildingObj_buildingId_edit").empty();
						var html="";
		        		$(buildings).each(function(i,building){
		        			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
		        		});
		        		$("#repair_buildingObj_buildingId_edit").html(html);
		        		$("#repair_buildingObj_buildingId_edit").val(repair.buildingObjPri);
					}
				});
				$("#repair_roomNo_edit").val(repair.roomNo);
				$.ajax({
					url: basePath + "RepairItem/listAll",
					type: "get",
					success: function(repairItems,response,status) { 
						$("#repair_repairItemObj_itemId_edit").empty();
						var html="";
		        		$(repairItems).each(function(i,repairItem){
		        			html += "<option value='" + repairItem.itemId + "'>" + repairItem.itemName + "</option>";
		        		});
		        		$("#repair_repairItemObj_itemId_edit").html(html);
		        		$("#repair_repairItemObj_itemId_edit").val(repair.repairItemObjPri);
					}
				});
				$("#repair_repairDesc_edit").val(repair.repairDesc);
				$.ajax({
					url: basePath + "Student/listAll",
					type: "get",
					success: function(students,response,status) { 
						$("#repair_studentObj_studentNo_edit").empty();
						var html="";
		        		$(students).each(function(i,student){
		        			html += "<option value='" + student.studentNo + "'>" + student.name + "</option>";
		        		});
		        		$("#repair_studentObj_studentNo_edit").html(html);
		        		$("#repair_studentObj_studentNo_edit").val(repair.studentObjPri);
					}
				});
				$("#repair_addTime_edit").val(repair.addTime);
				$.ajax({
					url: basePath + "RepairState/listAll",
					type: "get",
					success: function(repairStates,response,status) { 
						$("#repair_repairStateObj_stateId_edit").empty();
						var html="";
		        		$(repairStates).each(function(i,repairState){
		        			html += "<option value='" + repairState.stateId + "'>" + repairState.stateName + "</option>";
		        		});
		        		$("#repair_repairStateObj_stateId_edit").html(html);
		        		$("#repair_repairStateObj_stateId_edit").val(repair.repairStateObjPri);
					}
				});
				$("#repair_handleResult_edit").val(repair.handleResult);
				$('#repairEditDialog').modal('show');
			} else {
				alert("获取信息失败！");
			}
		}
	});
}

/*删除报修信息*/
function repairDelete(repairId) {
	if(confirm("确认删除这个记录")) {
		$.ajax({
			type : "POST",
			url : basePath + "Repair/deletes",
			data : {
				repairIds : repairId,
			},
			success : function (obj) {
				if (obj.success) {
					alert("删除成功");
					$("#repairQueryForm").submit();
					//location.href= basePath + "Repair/frontlist";
				}
				else 
					alert(obj.message);
			},
		});
	}
}

/*ajax方式提交报修信息表单给服务器端修改*/
function ajaxRepairModify() {
	$.ajax({
		url :  basePath + "Repair/" + $("#repair_repairId_edit").val() + "/update",
		type : "post",
		dataType: "json",
		data: new FormData($("#repairEditForm")[0]),
		success : function (obj, response, status) {
            if(obj.success){
                alert("信息修改成功！");
                $("#repairQueryForm").submit();
            }else{
                alert(obj.message);
            } 
		},
		processData: false,
		contentType: false,
	});
}

$(function(){
	/*小屏幕导航点击关闭菜单*/
    $('.navbar-collapse a').click(function(){
        $('.navbar-collapse').collapse('hide');
    });
    new WOW().init();

    /*上报时间组件*/
    $('.repair_addTime_edit').datetimepicker({
    	language:  'zh-CN',  //语言
    	format: 'yyyy-mm-dd hh:ii:ss',
    	weekStart: 1,
    	todayBtn:  1,
    	autoclose: 1,
    	minuteStep: 1,
    	todayHighlight: 1,
    	startView: 2,
    	forceParse: 0
    });
})
</script>
</body>
</html>

