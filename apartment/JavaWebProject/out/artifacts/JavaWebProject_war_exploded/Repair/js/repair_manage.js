﻿var repair_manage_tool = null; 
$(function () { 
	initRepairManageTool(); //建立Repair管理对象
	repair_manage_tool.init(); //如果需要通过下拉框查询，首先初始化下拉框的值
	$("#repair_manage").datagrid({
		url : 'Repair/list',
		fit : true,
		fitColumns : true,
		striped : true,
		rownumbers : true,
		border : false,
		pagination : true,
		pageSize : 5,
		pageList : [5, 10, 15, 20, 25],
		pageNumber : 1,
		sortName : "repairId",
		sortOrder : "desc",
		toolbar : "#repair_manage_tool",
		columns : [[
			{
				field : "repairId",
				title : "报修id",
				width : 70,
			},
			{
				field : "buildingObj",
				title : "公寓楼",
				width : 140,
			},
			{
				field : "roomNo",
				title : "宿舍号",
				width : 140,
			},
			{
				field : "repairItemObj",
				title : "报修项目",
				width : 140,
			},
			{
				field : "studentObj",
				title : "上报学生",
				width : 140,
			},
			{
				field : "addTime",
				title : "上报时间",
				width : 140,
			},
			{
				field : "repairStateObj",
				title : "维修状态",
				width : 140,
			},
		]],
	});

	$("#repairEditDiv").dialog({
		title : "修改管理",
		top: "50px",
		width : 700,
		height : 515,
		modal : true,
		closed : true,
		iconCls : "icon-edit-new",
		buttons : [{
			text : "提交",
			iconCls : "icon-edit-new",
			handler : function () {
				if ($("#repairEditForm").form("validate")) {
					//验证表单 
					if(!$("#repairEditForm").form("validate")) {
						$.messager.alert("错误提示","你输入的信息还有错误！","warning");
					} else {
						$("#repairEditForm").form({
						    url:"Repair/" + $("#repair_repairId_edit").val() + "/update",
						    onSubmit: function(){
								if($("#repairEditForm").form("validate"))  {
				                	$.messager.progress({
										text : "正在提交数据中...",
									});
				                	return true;
				                } else { 
				                    return false; 
				                }
						    },
						    success:function(data){
						    	$.messager.progress("close");
						    	console.log(data);
			                	var obj = jQuery.parseJSON(data);
			                    if(obj.success){
			                        $.messager.alert("消息","信息修改成功！");
			                        $("#repairEditDiv").dialog("close");
			                        repair_manage_tool.reload();
			                    }else{
			                        $.messager.alert("消息",obj.message);
			                    } 
						    }
						});
						//提交表单
						$("#repairEditForm").submit();
					}
				}
			},
		},{
			text : "取消",
			iconCls : "icon-redo",
			handler : function () {
				$("#repairEditDiv").dialog("close");
				$("#repairEditForm").form("reset"); 
			},
		}],
	});
});

function initRepairManageTool() {
	repair_manage_tool = {
		init: function() {
			$.ajax({
				url : "Building/listAll",
				type : "post",
				success : function (data, response, status) {
					$("#buildingObj_buildingId_query").combobox({ 
					    valueField:"buildingId",
					    textField:"buildingName",
					    panelHeight: "200px",
				        editable: false, //不允许手动输入 
					});
					data.splice(0,0,{buildingId:0,buildingName:"不限制"});
					$("#buildingObj_buildingId_query").combobox("loadData",data); 
				}
			});
			$.ajax({
				url : "RepairItem/listAll",
				type : "post",
				success : function (data, response, status) {
					$("#repairItemObj_itemId_query").combobox({ 
					    valueField:"itemId",
					    textField:"itemName",
					    panelHeight: "200px",
				        editable: false, //不允许手动输入 
					});
					data.splice(0,0,{itemId:0,itemName:"不限制"});
					$("#repairItemObj_itemId_query").combobox("loadData",data); 
				}
			});
			$.ajax({
				url : "Student/listAll",
				type : "post",
				success : function (data, response, status) {
					$("#studentObj_studentNo_query").combobox({ 
					    valueField:"studentNo",
					    textField:"name",
					    panelHeight: "200px",
				        editable: false, //不允许手动输入 
					});
					data.splice(0,0,{studentNo:"",name:"不限制"});
					$("#studentObj_studentNo_query").combobox("loadData",data); 
				}
			});
			$.ajax({
				url : "RepairState/listAll",
				type : "post",
				success : function (data, response, status) {
					$("#repairStateObj_stateId_query").combobox({ 
					    valueField:"stateId",
					    textField:"stateName",
					    panelHeight: "200px",
				        editable: false, //不允许手动输入 
					});
					data.splice(0,0,{stateId:0,stateName:"不限制"});
					$("#repairStateObj_stateId_query").combobox("loadData",data); 
				}
			});
		},
		reload : function () {
			$("#repair_manage").datagrid("reload");
		},
		redo : function () {
			$("#repair_manage").datagrid("unselectAll");
		},
		search: function() {
			var queryParams = $("#repair_manage").datagrid("options").queryParams;
			queryParams["buildingObj.buildingId"] = $("#buildingObj_buildingId_query").combobox("getValue");
			queryParams["roomNo"] = $("#roomNo").val();
			queryParams["repairItemObj.itemId"] = $("#repairItemObj_itemId_query").combobox("getValue");
			queryParams["studentObj.studentNo"] = $("#studentObj_studentNo_query").combobox("getValue");
			queryParams["addTime"] = $("#addTime").datebox("getValue"); 
			queryParams["repairStateObj.stateId"] = $("#repairStateObj_stateId_query").combobox("getValue");
			$("#repair_manage").datagrid("options").queryParams=queryParams; 
			$("#repair_manage").datagrid("load");
		},
		exportExcel: function() {
			$("#repairQueryForm").form({
			    url:"Repair/OutToExcel",
			});
			//提交表单
			$("#repairQueryForm").submit();
		},
		remove : function () {
			var rows = $("#repair_manage").datagrid("getSelections");
			if (rows.length > 0) {
				$.messager.confirm("确定操作", "您正在要删除所选的记录吗？", function (flag) {
					if (flag) {
						var repairIds = [];
						for (var i = 0; i < rows.length; i ++) {
							repairIds.push(rows[i].repairId);
						}
						$.ajax({
							type : "POST",
							url : "Repair/deletes",
							data : {
								repairIds : repairIds.join(","),
							},
							beforeSend : function () {
								$("#repair_manage").datagrid("loading");
							},
							success : function (data) {
								if (data.success) {
									$("#repair_manage").datagrid("loaded");
									$("#repair_manage").datagrid("load");
									$("#repair_manage").datagrid("unselectAll");
									$.messager.show({
										title : "提示",
										msg : data.message
									});
								} else {
									$("#repair_manage").datagrid("loaded");
									$("#repair_manage").datagrid("load");
									$("#repair_manage").datagrid("unselectAll");
									$.messager.alert("消息",data.message);
								}
							},
						});
					}
				});
			} else {
				$.messager.alert("提示", "请选择要删除的记录！", "info");
			}
		},
		edit : function () {
			var rows = $("#repair_manage").datagrid("getSelections");
			if (rows.length > 1) {
				$.messager.alert("警告操作！", "编辑记录只能选定一条数据！", "warning");
			} else if (rows.length == 1) {
				$.ajax({
					url : "Repair/" + rows[0].repairId +  "/update",
					type : "get",
					data : {
						//repairId : rows[0].repairId,
					},
					beforeSend : function () {
						$.messager.progress({
							text : "正在获取中...",
						});
					},
					success : function (repair, response, status) {
						$.messager.progress("close");
						if (repair) { 
							$("#repairEditDiv").dialog("open");
							$("#repair_repairId_edit").val(repair.repairId);
							$("#repair_repairId_edit").validatebox({
								required : true,
								missingMessage : "请输入报修id",
								editable: false
							});
							$("#repair_buildingObj_buildingId_edit").combobox({
								url:"Building/listAll",
							    valueField:"buildingId",
							    textField:"buildingName",
							    panelHeight: "auto",
						        editable: false, //不允许手动输入 
						        onLoadSuccess: function () { //数据加载完毕事件
									$("#repair_buildingObj_buildingId_edit").combobox("select", repair.buildingObjPri);
									//var data = $("#repair_buildingObj_buildingId_edit").combobox("getData"); 
						            //if (data.length > 0) {
						                //$("#repair_buildingObj_buildingId_edit").combobox("select", data[0].buildingId);
						            //}
								}
							});
							$("#repair_roomNo_edit").val(repair.roomNo);
							$("#repair_roomNo_edit").validatebox({
								required : true,
								missingMessage : "请输入宿舍号",
							});
							$("#repair_repairItemObj_itemId_edit").combobox({
								url:"RepairItem/listAll",
							    valueField:"itemId",
							    textField:"itemName",
							    panelHeight: "auto",
						        editable: false, //不允许手动输入 
						        onLoadSuccess: function () { //数据加载完毕事件
									$("#repair_repairItemObj_itemId_edit").combobox("select", repair.repairItemObjPri);
									//var data = $("#repair_repairItemObj_itemId_edit").combobox("getData"); 
						            //if (data.length > 0) {
						                //$("#repair_repairItemObj_itemId_edit").combobox("select", data[0].itemId);
						            //}
								}
							});
							$("#repair_repairDesc_edit").val(repair.repairDesc);
							$("#repair_repairDesc_edit").validatebox({
								required : true,
								missingMessage : "请输入问题描述",
							});
							$("#repair_studentObj_studentNo_edit").combobox({
								url:"Student/listAll",
							    valueField:"studentNo",
							    textField:"name",
							    panelHeight: "auto",
						        editable: false, //不允许手动输入 
						        onLoadSuccess: function () { //数据加载完毕事件
									$("#repair_studentObj_studentNo_edit").combobox("select", repair.studentObjPri);
									//var data = $("#repair_studentObj_studentNo_edit").combobox("getData"); 
						            //if (data.length > 0) {
						                //$("#repair_studentObj_studentNo_edit").combobox("select", data[0].studentNo);
						            //}
								}
							});
							$("#repair_addTime_edit").datetimebox({
								value: repair.addTime,
							    required: true,
							    showSeconds: true,
							});
							$("#repair_repairStateObj_stateId_edit").combobox({
								url:"RepairState/listAll",
							    valueField:"stateId",
							    textField:"stateName",
							    panelHeight: "auto",
						        editable: false, //不允许手动输入 
						        onLoadSuccess: function () { //数据加载完毕事件
									$("#repair_repairStateObj_stateId_edit").combobox("select", repair.repairStateObjPri);
									//var data = $("#repair_repairStateObj_stateId_edit").combobox("getData"); 
						            //if (data.length > 0) {
						                //$("#repair_repairStateObj_stateId_edit").combobox("select", data[0].stateId);
						            //}
								}
							});
							$("#repair_handleResult_edit").val(repair.handleResult);
						} else {
							$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
						}
					}
				});
			} else if (rows.length == 0) {
				$.messager.alert("警告操作！", "编辑记录至少选定一条数据！", "warning");
			}
		},
	};
}
