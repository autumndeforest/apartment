﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/repair.css" />
<div id="repairAddDiv">
	<form id="repairAddForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">公寓楼:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_buildingObj_buildingId" name="repair.buildingObj.buildingId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">宿舍号:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_roomNo" name="repair.roomNo" style="width:200px" />

			</span>

		</div>
		<div>
			<span class="label">报修项目:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_repairItemObj_itemId" name="repair.repairItemObj.itemId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">问题描述:</span>
			<span class="inputControl">
				<textarea id="repair_repairDesc" name="repair.repairDesc" rows="6" cols="80"></textarea>

			</span>

		</div>
		<div>
			<span class="label">上报学生:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_studentObj_studentNo" name="repair.studentObj.studentNo" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">上报时间:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_addTime" name="repair.addTime" />

			</span>

		</div>
		<div>
			<span class="label">维修状态:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_repairStateObj_stateId" name="repair.repairStateObj.stateId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">处理结果:</span>
			<span class="inputControl">
				<textarea id="repair_handleResult" name="repair.handleResult" rows="6" cols="80"></textarea>

			</span>

		</div>
		<div class="operation">
			<a id="repairAddButton" class="easyui-linkbutton">添加</a>
			<a id="repairClearButton" class="easyui-linkbutton">重填</a>
		</div> 
	</form>
</div>
<script src="${pageContext.request.contextPath}/Repair/js/repair_add.js"></script> 
