﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Building" %>
<%@ page import="com.org.po.RepairItem" %>
<%@ page import="com.org.po.RepairState" %>
<%@ page import="com.org.po.Student" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
<title>报修添加</title>
<link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
<link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
<link href="<%=basePath %>plugins/animate.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body style="margin-top:70px;">
<jsp:include page="../header.jsp"></jsp:include>
<div class="container">
	<div class="row">
		<div class="col-md-12 wow fadeInUp" data-wow-duration="0.5s">
			<div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
			    	<li role="presentation" ><a href="<%=basePath %>Repair/frontlist">报修列表</a></li>
			    	<li role="presentation" class="active"><a href="#repairAdd" aria-controls="repairAdd" role="tab" data-toggle="tab">添加报修</a></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
				    <div role="tabpanel" class="tab-pane" id="repairList">
				    </div>
				    <div role="tabpanel" class="tab-pane active" id="repairAdd"> 
				      	<form class="form-horizontal" name="repairAddForm" id="repairAddForm" enctype="multipart/form-data" method="post"  class="mar_t15">
						  <div class="form-group">
						  	 <label for="repair_buildingObj_buildingId" class="col-md-2 text-right">公寓楼:</label>
						  	 <div class="col-md-8">
							    <select id="repair_buildingObj_buildingId" name="repair.buildingObj.buildingId" class="form-control">
							    </select>
						  	 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="repair_roomNo" class="col-md-2 text-right">宿舍号:</label>
						  	 <div class="col-md-8">
							    <input type="text" id="repair_roomNo" name="repair.roomNo" class="form-control" placeholder="请输入宿舍号">
							 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="repair_repairItemObj_itemId" class="col-md-2 text-right">报修项目:</label>
						  	 <div class="col-md-8">
							    <select id="repair_repairItemObj_itemId" name="repair.repairItemObj.itemId" class="form-control">
							    </select>
						  	 </div>
						  </div>
						  <div class="form-group">
						  	 <label for="repair_repairDesc" class="col-md-2 text-right">问题描述:</label>
						  	 <div class="col-md-8">
							    <textarea id="repair_repairDesc" name="repair.repairDesc" rows="8" class="form-control" placeholder="请输入问题描述"></textarea>
							 </div>
						  </div>
						  <div class="form-group" style="display:none;">
						  	 <label for="repair_studentObj_studentNo" class="col-md-2 text-right">上报学生:</label>
						  	 <div class="col-md-8">
							    <select id="repair_studentObj_studentNo" name="repair.studentObj.studentNo" class="form-control">
							    </select>
						  	 </div>
						  </div>
						  <div class="form-group" style="display:none;">
						  	 <label for="repair_addTimeDiv" class="col-md-2 text-right">上报时间:</label>
						  	 <div class="col-md-8">
				                <div id="repair_addTimeDiv" class="input-group date repair_addTime col-md-12" data-link-field="repair_addTime">
				                    <input class="form-control" id="repair_addTime" name="repair.addTime" size="16" type="text" value="" placeholder="请选择上报时间" readonly>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
				                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
				                </div>
						  	 </div>
						  </div>
						  <div class="form-group" style="display:none;">
						  	 <label for="repair_repairStateObj_stateId" class="col-md-2 text-right">维修状态:</label>
						  	 <div class="col-md-8">
							    <select id="repair_repairStateObj_stateId" name="repair.repairStateObj.stateId" class="form-control">
							    </select>
						  	 </div>
						  </div>
						  <div class="form-group" style="display:none;">
						  	 <label for="repair_handleResult" class="col-md-2 text-right">处理结果:</label>
						  	 <div class="col-md-8">
							    <textarea id="repair_handleResult" name="repair.handleResult" rows="8" class="form-control" placeholder="请输入处理结果"></textarea>
							 </div>
						  </div>
				          <div class="form-group">
				             <span class="col-md-2""></span>
				             <span onclick="ajaxRepairAdd();" class="btn btn-primary bottom5 top5">我要报修</span>
				          </div>
						</form> 
				        <style>#repairAddForm .form-group {margin:10px;}  </style>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>

<jsp:include page="../footer.jsp"></jsp:include> 
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script>
var basePath = "<%=basePath%>";
	//提交添加报修信息
	function ajaxRepairAdd() { 
		//提交之前先验证表单
		$("#repairAddForm").data('bootstrapValidator').validate();
		if(!$("#repairAddForm").data('bootstrapValidator').isValid()){
			return;
		}
		jQuery.ajax({
			type : "post",
			url : basePath + "Repair/userAdd",
			dataType : "json" , 
			data: new FormData($("#repairAddForm")[0]),
			success : function(obj) {
				if(obj.success){ 
					alert("保存成功！");
					$("#repairAddForm").find("input").val("");
					$("#repairAddForm").find("textarea").val("");
				} else {
					alert(obj.message);
				}
			},
			processData: false, 
			contentType: false, 
		});
	} 
$(function(){
	/*小屏幕导航点击关闭菜单*/
    $('.navbar-collapse a').click(function(){
        $('.navbar-collapse').collapse('hide');
    });
    new WOW().init();
	//验证报修添加表单字段
	$('#repairAddForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			"repair.roomNo": {
				validators: {
					notEmpty: {
						message: "宿舍号不能为空",
					}
				}
			},
			"repair.repairDesc": {
				validators: {
					notEmpty: {
						message: "问题描述不能为空",
					}
				}
			},
		 
		}
	}); 
	//初始化公寓楼下拉框值
	$.ajax({
		url: basePath + "Building/listAll",
		type: "get",
		success: function(buildings,response,status) { 
			$("#repair_buildingObj_buildingId").empty();
			var html="";
    		$(buildings).each(function(i,building){
    			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
    		});
    		$("#repair_buildingObj_buildingId").html(html);
    	}
	});
	//初始化报修项目下拉框值 
	$.ajax({
		url: basePath + "RepairItem/listAll",
		type: "get",
		success: function(repairItems,response,status) { 
			$("#repair_repairItemObj_itemId").empty();
			var html="";
    		$(repairItems).each(function(i,repairItem){
    			html += "<option value='" + repairItem.itemId + "'>" + repairItem.itemName + "</option>";
    		});
    		$("#repair_repairItemObj_itemId").html(html);
    	}
	});
	//初始化上报学生下拉框值 
	$.ajax({
		url: basePath + "Student/listAll",
		type: "get",
		success: function(students,response,status) { 
			$("#repair_studentObj_studentNo").empty();
			var html="";
    		$(students).each(function(i,student){
    			html += "<option value='" + student.studentNo + "'>" + student.name + "</option>";
    		});
    		$("#repair_studentObj_studentNo").html(html);
    	}
	});
	//初始化维修状态下拉框值 
	$.ajax({
		url: basePath + "RepairState/listAll",
		type: "get",
		success: function(repairStates,response,status) { 
			$("#repair_repairStateObj_stateId").empty();
			var html="";
    		$(repairStates).each(function(i,repairState){
    			html += "<option value='" + repairState.stateId + "'>" + repairState.stateName + "</option>";
    		});
    		$("#repair_repairStateObj_stateId").html(html);
    	}
	});
	//上报时间组件
	$('#repair_addTimeDiv').datetimepicker({
		language:  'zh-CN',  //显示语言
		format: 'yyyy-mm-dd hh:ii:ss',
		weekStart: 1,
		todayBtn:  1,
		autoclose: 1,
		minuteStep: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0
	}).on('hide',function(e) {
		//下面这行代码解决日期组件改变日期后不验证的问题
		$('#repairAddForm').data('bootstrapValidator').updateStatus('repair.addTime', 'NOT_VALIDATED',null).validateField('repair.addTime');
	});
})
</script>
</body>
</html>
