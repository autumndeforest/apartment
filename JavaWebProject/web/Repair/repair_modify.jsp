﻿<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/repair.css" />
<div id="repair_editDiv">
	<form id="repairEditForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">报修id:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_repairId_edit" name="repair.repairId" value="<%=request.getParameter("repairId") %>" style="width:200px" />
			</span>
		</div>

		<div>
			<span class="label">公寓楼:</span>
			<span class="inputControl">
				<input class="textbox"  id="repair_buildingObj_buildingId_edit" name="repair.buildingObj.buildingId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">宿舍号:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_roomNo_edit" name="repair.roomNo" style="width:200px" />

			</span>

		</div>
		<div>
			<span class="label">报修项目:</span>
			<span class="inputControl">
				<input class="textbox"  id="repair_repairItemObj_itemId_edit" name="repair.repairItemObj.itemId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">问题描述:</span>
			<span class="inputControl">
				<textarea id="repair_repairDesc_edit" name="repair.repairDesc" rows="8" cols="60"></textarea>

			</span>

		</div>
		<div>
			<span class="label">上报学生:</span>
			<span class="inputControl">
				<input class="textbox"  id="repair_studentObj_studentNo_edit" name="repair.studentObj.studentNo" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">上报时间:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repair_addTime_edit" name="repair.addTime" />

			</span>

		</div>
		<div>
			<span class="label">维修状态:</span>
			<span class="inputControl">
				<input class="textbox"  id="repair_repairStateObj_stateId_edit" name="repair.repairStateObj.stateId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">处理结果:</span>
			<span class="inputControl">
				<textarea id="repair_handleResult_edit" name="repair.handleResult" rows="8" cols="60"></textarea>

			</span>

		</div>
		<div class="operation">
			<a id="repairModifyButton" class="easyui-linkbutton">更新</a> 
		</div>
	</form>
</div>
<script src="${pageContext.request.contextPath}/Repair/js/repair_modify.js"></script> 
