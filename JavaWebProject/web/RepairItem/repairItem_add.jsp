﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/repairItem.css" />
<div id="repairItemAddDiv">
	<form id="repairItemAddForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">项目名称:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repairItem_itemName" name="repairItem.itemName" style="width:200px" />

			</span>

		</div>
		<div class="operation">
			<a id="repairItemAddButton" class="easyui-linkbutton">添加</a>
			<a id="repairItemClearButton" class="easyui-linkbutton">重填</a>
		</div> 
	</form>
</div>
<script src="${pageContext.request.contextPath}/RepairItem/js/repairItem_add.js"></script> 
