﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
    <title>学生公寓管理系统-首页</title>
    <link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
    <link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
    <link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
    <link href="<%=basePath %>css/falsh.css" rel="stylesheet">
    <link href="<%=basePath %>css/preview.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="preview.css"/>
    <script src="./preview.js"></script>

</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"></jsp:include>
    <!-- 广告轮播开始 -->
    <section id="main_ad" class="carousel slide" data-ride="carousel">
        <!-- 下面的小点点，活动指示器 -->
        <ol class="carousel-indicators">
            <li data-target="#main_ad" data-slide-to="0" class="active"></li>
            <li data-target="#main_ad" data-slide-to="1"></li>
            <li data-target="#main_ad" data-slide-to="2"></li>
            <li data-target="#main_ad" data-slide-to="3"></li>
        </ol>
        <!-- 轮播项 -->
        <div class="carousel-inner" role="listbox">
            <div class="item active" data-image-lg="<%=basePath %>images/slider/slide_01_2000x410.jpg"
                 data-image-xs="<%=basePath %>images/slider/slide_01_640x340.jpg"></div>
            <div class="item" data-image-lg="<%=basePath %>images/slider/slide_02_2000x410.jpg"
                 data-image-xs="<%=basePath %>images/slider/slide_02_640x340.jpg"></div>
            <div class="item" data-image-lg="<%=basePath %>images/slider/slide_03_2000x410.jpg"
                 data-image-xs="<%=basePath %>images/slider/slide_03_640x340.jpg"></div>
            <div class="item" data-image-lg="<%=basePath %>images/slider/slide_04_2000x410.jpg"
                 data-image-xs="<%=basePath %>images/slider/slide_04_640x340.jpg"></div>
        </div>
        <!-- 控制按钮 -->
        <a class="left carousel-control" href="#main_ad" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">上一页</span>
        </a>
        <a class="right carousel-control" href="#main_ad" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">下一页</span>
        </a>
    </section>
    <!-- /广告轮播结束 -->

    <!-- 特色介绍开始 -->

    <section id="tese">
        <div class="container">
            <div class="row">

                <abbr title="向左移动并加速" href="javascript:;">←</abbr>
                <abbr title="向右移动并加速" href="javascript:;">→</abbr>
                <div id="div1">

                    <ul id="wrap">

                        <li><img src="upload/school_spirit.png"></li>
                        <li><img src="upload/yayuan9.png"></li>
                        <li><img src="upload/jingyuan.png"></li>
                        <li><img src="upload/yiyuan.png"></li>
                        <li><img src="upload/playground.jpg"></li>
                        <li><img src="upload/room.jpg"></li>
                        <li><img src="upload/jply.jpg"></li>
                    </ul>
                </div>

            </div>
        </div>
    </section>
    <!-- /特色介绍 结束-->

    <jsp:include page="footer.jsp"></jsp:include>

</div>
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>js/index.js"></script>
<script src="<%=basePath %>js/preview.js"></script>
</body>
<script>
    var preview = new Preview({
        imgWrap: 'wrap' // 指定该容器里的图片点击预览
    })
</script>
</html>