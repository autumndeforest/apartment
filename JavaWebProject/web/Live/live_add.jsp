﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/live.css" />
<div id="liveAddDiv">
	<form id="liveAddForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">入住公寓楼:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_buildingObj_buildingId" name="live.buildingObj.buildingId" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">入住宿舍号:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_roomNo" name="live.roomNo" style="width:200px" />

			</span>

		</div>
		<div>
			<span class="label">入住学生:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_studentObj_studentNo" name="live.studentObj.studentNo" style="width: auto"/>
			</span>
		</div>
		<div>
			<span class="label">入住日期:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="live_inDate" name="live.inDate" />

			</span>

		</div>
		<div>
			<span class="label">备注信息:</span>
			<span class="inputControl">
				<textarea id="live_liveMemo" name="live.liveMemo" rows="6" cols="80"></textarea>

			</span>

		</div>
		<div class="operation">
			<a id="liveAddButton" class="easyui-linkbutton">添加</a>
			<a id="liveClearButton" class="easyui-linkbutton">重填</a>
		</div> 
	</form>
</div>
<script src="${pageContext.request.contextPath}/Live/js/live_add.js"></script> 
