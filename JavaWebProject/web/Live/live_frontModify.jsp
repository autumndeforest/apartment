﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Live" %>
<%@ page import="com.org.po.Building" %>
<%@ page import="com.org.po.Student" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    //获取所有的buildingObj信息
    List<Building> buildingList = (List<Building>)request.getAttribute("buildingList");
    //获取所有的studentObj信息
    List<Student> studentList = (List<Student>)request.getAttribute("studentList");
    Live live = (Live)request.getAttribute("live");

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
  <TITLE>修改住宿信息</TITLE>
  <link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/animate.css" rel="stylesheet"> 
</head>
<body style="margin-top:70px;"> 
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="col-md-9 wow fadeInLeft">
	<ul class="breadcrumb">
  		<li><a href="<%=basePath %>index.jsp">首页</a></li>
  		<li class="active">住宿信息修改</li>
	</ul>
		<div class="row"> 
      	<form class="form-horizontal" name="liveEditForm" id="liveEditForm" enctype="multipart/form-data" method="post"  class="mar_t15">
		  <div class="form-group">
			 <label for="live_liveId_edit" class="col-md-3 text-right">记录id:</label>
			 <div class="col-md-9"> 
			 	<input type="text" id="live_liveId_edit" name="live.liveId" class="form-control" placeholder="请输入记录id" readOnly>
			 </div>
		  </div> 
		  <div class="form-group">
		  	 <label for="live_buildingObj_buildingId_edit" class="col-md-3 text-right">入住公寓楼:</label>
		  	 <div class="col-md-9">
			    <select id="live_buildingObj_buildingId_edit" name="live.buildingObj.buildingId" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_roomNo_edit" class="col-md-3 text-right">入住宿舍号:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="live_roomNo_edit" name="live.roomNo" class="form-control" placeholder="请输入入住宿舍号">
			 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_studentObj_studentNo_edit" class="col-md-3 text-right">入住学生:</label>
		  	 <div class="col-md-9">
			    <select id="live_studentObj_studentNo_edit" name="live.studentObj.studentNo" class="form-control">
			    </select>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_inDate_edit" class="col-md-3 text-right">入住日期:</label>
		  	 <div class="col-md-9">
                <div class="input-group date live_inDate_edit col-md-12" data-link-field="live_inDate_edit" data-link-format="yyyy-mm-dd">
                    <input class="form-control" id="live_inDate_edit" name="live.inDate" size="16" type="text" value="" placeholder="请选择入住日期" readonly>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
		  	 </div>
		  </div>
		  <div class="form-group">
		  	 <label for="live_liveMemo_edit" class="col-md-3 text-right">备注信息:</label>
		  	 <div class="col-md-9">
			    <textarea id="live_liveMemo_edit" name="live.liveMemo" rows="8" class="form-control" placeholder="请输入备注信息"></textarea>
			 </div>
		  </div>
			  <div class="form-group">
			  	<span class="col-md-3""></span>
			  	<span onclick="ajaxLiveModify();" class="btn btn-primary bottom5 top5">修改</span>
			  </div>
		</form> 
	    <style>#liveEditForm .form-group {margin-bottom:5px;}  </style>
      </div>
   </div>
</div>


<jsp:include page="../footer.jsp"></jsp:include>
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jsdate.js"></script>
<script>
var basePath = "<%=basePath%>";
/*弹出修改住宿界面并初始化数据*/
function liveEdit(liveId) {
	$.ajax({
		url :  basePath + "Live/" + liveId + "/update",
		type : "get",
		dataType: "json",
		success : function (live, response, status) {
			if (live) {
				$("#live_liveId_edit").val(live.liveId);
				$.ajax({
					url: basePath + "Building/listAll",
					type: "get",
					success: function(buildings,response,status) { 
						$("#live_buildingObj_buildingId_edit").empty();
						var html="";
		        		$(buildings).each(function(i,building){
		        			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
		        		});
		        		$("#live_buildingObj_buildingId_edit").html(html);
		        		$("#live_buildingObj_buildingId_edit").val(live.buildingObjPri);
					}
				});
				$("#live_roomNo_edit").val(live.roomNo);
				$.ajax({
					url: basePath + "Student/listAll",
					type: "get",
					success: function(students,response,status) { 
						$("#live_studentObj_studentNo_edit").empty();
						var html="";
		        		$(students).each(function(i,student){
		        			html += "<option value='" + student.studentNo + "'>" + student.name + "</option>";
		        		});
		        		$("#live_studentObj_studentNo_edit").html(html);
		        		$("#live_studentObj_studentNo_edit").val(live.studentObjPri);
					}
				});
				$("#live_inDate_edit").val(live.inDate);
				$("#live_liveMemo_edit").val(live.liveMemo);
			} else {
				alert("获取信息失败！");
			}
		}
	});
}

/*ajax方式提交住宿信息表单给服务器端修改*/
function ajaxLiveModify() {
	$.ajax({
		url :  basePath + "Live/" + $("#live_liveId_edit").val() + "/update",
		type : "post",
		dataType: "json",
		data: new FormData($("#liveEditForm")[0]),
		success : function (obj, response, status) {
            if(obj.success){
                alert("信息修改成功！");
                location.reload(true);
                $("#liveQueryForm").submit();
            }else{
                alert(obj.message);
            } 
		},
		processData: false,
		contentType: false,
	});
}

$(function(){
        /*小屏幕导航点击关闭菜单*/
        $('.navbar-collapse a').click(function(){
            $('.navbar-collapse').collapse('hide');
        });
        new WOW().init();
    /*入住日期组件*/
    $('.live_inDate_edit').datetimepicker({
    	language:  'zh-CN',  //语言
    	format: 'yyyy-mm-dd',
    	minView: 2,
    	weekStart: 1,
    	todayBtn:  1,
    	autoclose: 1,
    	minuteStep: 1,
    	todayHighlight: 1,
    	startView: 2,
    	forceParse: 0
    });
    liveEdit("<%=request.getParameter("liveId")%>");
 })
 </script> 
</body>
</html>

