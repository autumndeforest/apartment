function savePaw() {
	if (!$("#saveid").prop("checked")) {
		$.cookie('manager', '', {
			expires : -1
		});
		$("#manager").val('');
	}
}


function saveCookie() {
	if ($("#saveid").prop("checked")) {
		$.cookie('manager', $("#manager").val(), {
			expires : 7
		});
	}
}

$(function () {

	//登录界面
	$('#login').dialog({
		title : '登录后台',
		width : 300,
		height : 215,
		modal : true,
		iconCls : 'icon-login',
		buttons : '#btn',
	});
	
	//管理员帐号验证
	$('#manager').validatebox({
		required : true,
		missingMessage : '请输入管理员帐号',
		invalidMessage : '管理员帐号不得为空',
	});
	
	//管理员密码验证
	$('#password').validatebox({
		required : true,
		validType : 'length[1,30]',
		missingMessage : '请输入管理员密码',
		invalidMessage : '管理员密码长度1-30',
	});
	
	//加载时判断验证
	if (!$('#manager').validatebox('isValid')) {
		$('#manager').focus();
	} else if (!$('#password').validatebox('isValid')) {
		$('#password').focus();
	}



	//单击登录
	$('#btn a').click(function () {
        if(!myRotateVerify.verifyState){
            /*alert(myRotateVerify.verifyState);*/
            alert("请先滑动图片");
            return;
        };
		if (!$('#manager').validatebox('isValid')) {
			$('#manager').focus();
		} else if (!$('#password').validatebox('isValid')) {
			$('#password').focus();
		} else {
			$.ajax({
				url : 'login',
				type : 'post',
				data : {
					"username" : $('#manager').val(),
					"password" : $('#password').val(),
				},
				beforeSend : function () {
					$.messager.progress({
						text : '正在登录中...',
					});
				},
				success : function (data, response, status) {
					$.messager.progress('close');  
					var obj = eval(data);
					if (obj.success) {
						saveCookie();
						location.href = 'main.jsp';;
					} else {
						$.messager.alert('登录失败！', obj.msg, 'warning', function () {
							$('#password').select();
						});
					}
				}
			});
		}
	});



    //登录验证
    var myRotateVerify = new RotateVerify('#rotateWrap', {
        initText: '滑动将图片转正',//默认
        slideImage: ['images/1.jpg', 'images/2.jpg', 'images/4.jpg'],//arr  [imgsrc1,imgsrc2] 或者str 'imgsrc1'
        slideAreaNum: 10,// 误差范围角度 +- 10(默认)
        getSuccessState: function (res) {//验证通过 返回  true;
            console.log('例1' + res);
        }
    })
    //重置
    $("#resetBtn").on('click', function () {
        myRotateVerify.resetSlide();
    })
    //可拿到 验证状态
    $("#testBtn").on('click', function () {
        alert(myRotateVerify.verifyState);
    })
});






