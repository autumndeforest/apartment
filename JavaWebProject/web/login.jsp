﻿<%@ page language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
    <title>信息管理系统_用户登录</title>

    <link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css"/>
    <link rel="stylesheet" type="text/css" href="easyui/themes/icon.css"/>
    <link rel="stylesheet" type="text/css" href="css/login.css"/>
    <link rel="stylesheet" type="text/css" href="css/style2.0.css"/>
</head>

<style type="text/css">
    ul li {
        font-size: 30px;
        color: #2ec0f6;
    }

    .tyg-div {
        z-index: -1000;
        float: left;
        position: absolute;
        left: 5%;
        top: 20%;
    }

    .tyg-p {
        font-size: 14px;
        font-family: 'microsoft yahei';
        position: absolute;
        top: 8px;
        left: 60px;
    }

    .tyg-div-denglv {
        z-index: 1000;
        float: right;
        position: absolute;
        right: 3%;
        top: 10%;
    }

    .tyg-div-form {
        background-color: #23305a;
        width: 300px;
        height: 610px;
        margin: 0 auto 0 auto;
        color: #2ec0f6;
    }

    .tyg-div-form form {
        padding: 10px;
    }

    .tyg-div-form form input[type="text"] {
        width: 270px;
        height: 30px;
        margin: 25px 10px 0px 0px;
    }

    .tyg-div-form form input[type="password"] {
        width: 270px;
        height: 30px;
        margin: 25px 10px 0px 0px;
    }

    .tyg-div-form form a {
        cursor: pointer;
        width: 270px;
        height: 44px;
        margin-top: 25px;
        padding: 0;
        background: #2ec0f6;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        border: 1px solid #2ec0f6;
        -moz-box-shadow: 0 15px 30px 0 rgba(255, 255, 255, .25) inset,
        0 2px 7px 0 rgba(0, 0, 0, .2);
        -webkit-box-shadow: 0 15px 30px 0 rgba(255, 255, 255, .25) inset,
        0 2px 7px 0 rgba(0, 0, 0, .2);
        box-shadow: 0 15px 30px 0 rgba(255, 255, 255, .25) inset,
        0 2px 7px 0 rgba(0, 0, 0, .2);
        font-family: 'PT Sans', Helvetica, Arial, sans-serif;
        font-size: 14px;
        font-weight: 700;
        color: #fff;
        text-shadow: 0 1px 2px rgba(0, 0, 0, .1);
        -o-transition: all .2s;
        -moz-transition: all .2s;
        -webkit-transition: all .2s;
        -ms-transition: all .2s;
    }
</style>

<body <%--style=" background-image:url(upload/tsg2.jpg);width: 1536px ;"--%>>

<div class="tyg-div">
    <ul>
        <li>让</li>
        <li>
            <div style="margin-left:20px;">数</div>
        </li>
        <li>
            <div style="margin-left:40px;">据</div>
        </li>
        <li>
            <div style="margin-left:60px;">改</div>
        </li>
        <li>
            <div style="margin-left:80px;">变</div>
        </li>
        <li>
            <div style="margin-left:100px;">生</div>
        </li>
        <li>
            <div style="margin-left:120px;">活</div>
        </li>
    </ul>
</div>
<div id="contPar" class="contPar">
    <div id="page1" style="z-index:1;">
        <div class="title0">学生公寓管理系统</div>
        <%--<div class="title1">旅游、交通、气象、公共安全、大数据</div>--%>
        <div class="imgGroug">
            <ul>
                <img alt="" class="img0 png" src="images/img/page1_0.png">
                <img alt="" class="img1 png" src="images/img/page1_1.png">
                <img alt="" class="img2 png" src="images/img/page1_2.png">
            </ul>
        </div>
        <img alt="" class="img3 png" src="images/img/page1_3.jpg">
    </div>
</div>
<div class="tyg-div-denglv">
    <div class="tyg-div-form">
        <form action="">
            <h2>后台登录</h2>
            <p class="tyg-p">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;欢迎访问学生公寓管理系统</p>
            <div style="margin:5px 0px;">
                <input id="manager" type="text" placeholder="请输入账号..."/>
            </div>
            <div style="margin:5px 0px;">
                <input id="password" type="password" placeholder="请输入密码..."/>
            </div>
            <div id="rotateWrap" style="margin-top:50px;">

            </div>
            <div style="text-align: center;margin-top: 50px;">
            <%--    <button type="button" id="resetBtn" style="height: 30px;">重置</button>
                <button type="button" id="testBtn" style="height: 30px;">状态测试</button>--%>
            <div id="btn">
                <a href="#" class="easyui-linkbutton"
                   style="padding: 0;width: 280px;/*margin-left: 4px;*/background: #2ec0f6;border: 1px solid #2ec0f6;">
                    <div style="color: white;font-size:16px;line-height: 30px;width: 280px;height: 50px;/*background: #2ec0f6;border: 1px solid #2ec0f6;*/border-radius: 6px;cursor: pointer;">
                        <strong style="line-height: 50px;">登录</strong>
                    </div>
                </a>
            </div>
        </form>
    </div>
</div>

<%--<script type="text/javascript" src="easyui/jquery.min.js"></script>--%>
<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="easyui/jquery.cookie.js"></script>
<script type="text/javascript" src="js/login.js"></script>
<script type="text/javascript" src="js/com.js"></script>
<script type="text/javascript" src="js/jqRotateVerify.js"></script>
</body>
</html>