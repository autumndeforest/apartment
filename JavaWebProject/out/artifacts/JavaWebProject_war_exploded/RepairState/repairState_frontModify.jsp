﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.RepairState" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    RepairState repairState = (RepairState)request.getAttribute("repairState");

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
  <TITLE>修改报修状态信息</TITLE>
  <link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
  <link href="<%=basePath %>plugins/animate.css" rel="stylesheet"> 
</head>
<body style="margin-top:70px;"> 
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="col-md-9 wow fadeInLeft">
	<ul class="breadcrumb">
  		<li><a href="<%=basePath %>index.jsp">首页</a></li>
  		<li class="active">报修状态信息修改</li>
	</ul>
		<div class="row"> 
      	<form class="form-horizontal" name="repairStateEditForm" id="repairStateEditForm" enctype="multipart/form-data" method="post"  class="mar_t15">
		  <div class="form-group">
			 <label for="repairState_stateId_edit" class="col-md-3 text-right">状态id:</label>
			 <div class="col-md-9"> 
			 	<input type="text" id="repairState_stateId_edit" name="repairState.stateId" class="form-control" placeholder="请输入状态id" readOnly>
			 </div>
		  </div> 
		  <div class="form-group">
		  	 <label for="repairState_stateName_edit" class="col-md-3 text-right">状态名称:</label>
		  	 <div class="col-md-9">
			    <input type="text" id="repairState_stateName_edit" name="repairState.stateName" class="form-control" placeholder="请输入状态名称">
			 </div>
		  </div>
			  <div class="form-group">
			  	<span class="col-md-3""></span>
			  	<span onclick="ajaxRepairStateModify();" class="btn btn-primary bottom5 top5">修改</span>
			  </div>
		</form> 
	    <style>#repairStateEditForm .form-group {margin-bottom:5px;}  </style>
      </div>
   </div>
</div>


<jsp:include page="../footer.jsp"></jsp:include>
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script type="text/javascript" src="<%=basePath %>js/jsdate.js"></script>
<script>
var basePath = "<%=basePath%>";
/*弹出修改报修状态界面并初始化数据*/
function repairStateEdit(stateId) {
	$.ajax({
		url :  basePath + "RepairState/" + stateId + "/update",
		type : "get",
		dataType: "json",
		success : function (repairState, response, status) {
			if (repairState) {
				$("#repairState_stateId_edit").val(repairState.stateId);
				$("#repairState_stateName_edit").val(repairState.stateName);
			} else {
				alert("获取信息失败！");
			}
		}
	});
}

/*ajax方式提交报修状态信息表单给服务器端修改*/
function ajaxRepairStateModify() {
	$.ajax({
		url :  basePath + "RepairState/" + $("#repairState_stateId_edit").val() + "/update",
		type : "post",
		dataType: "json",
		data: new FormData($("#repairStateEditForm")[0]),
		success : function (obj, response, status) {
            if(obj.success){
                alert("信息修改成功！");
                location.reload(true);
                location.href= basePath + "RepairState/frontlist";
            }else{
                alert(obj.message);
            } 
		},
		processData: false,
		contentType: false,
	});
}

$(function(){
        /*小屏幕导航点击关闭菜单*/
        $('.navbar-collapse a').click(function(){
            $('.navbar-collapse').collapse('hide');
        });
        new WOW().init();
    repairStateEdit("<%=request.getParameter("stateId")%>");
 })
 </script> 
</body>
</html>

