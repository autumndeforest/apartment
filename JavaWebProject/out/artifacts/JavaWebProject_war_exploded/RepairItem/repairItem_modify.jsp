﻿<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<jsp:include page="../check_logstate.jsp"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/repairItem.css" />
<div id="repairItem_editDiv">
	<form id="repairItemEditForm" enctype="multipart/form-data"  method="post">
		<div>
			<span class="label">项目id:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repairItem_itemId_edit" name="repairItem.itemId" value="<%=request.getParameter("itemId") %>" style="width:200px" />
			</span>
		</div>

		<div>
			<span class="label">项目名称:</span>
			<span class="inputControl">
				<input class="textbox" type="text" id="repairItem_itemName_edit" name="repairItem.itemName" style="width:200px" />

			</span>

		</div>
		<div class="operation">
			<a id="repairItemModifyButton" class="easyui-linkbutton">更新</a> 
		</div>
	</form>
</div>
<script src="${pageContext.request.contextPath}/RepairItem/js/repairItem_modify.js"></script> 
