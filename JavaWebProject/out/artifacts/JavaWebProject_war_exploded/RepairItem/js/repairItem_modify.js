﻿$(function () {
	$.ajax({
		url : "RepairItem/" + $("#repairItem_itemId_edit").val() + "/update",
		type : "get",
		data : {
			//itemId : $("#repairItem_itemId_edit").val(),
		},
		beforeSend : function () {
			$.messager.progress({
				text : "正在获取中...",
			});
		},
		success : function (repairItem, response, status) {
			$.messager.progress("close");
			if (repairItem) { 
				$("#repairItem_itemId_edit").val(repairItem.itemId);
				$("#repairItem_itemId_edit").validatebox({
					required : true,
					missingMessage : "请输入项目id",
					editable: false
				});
				$("#repairItem_itemName_edit").val(repairItem.itemName);
				$("#repairItem_itemName_edit").validatebox({
					required : true,
					missingMessage : "请输入项目名称",
				});
			} else {
				$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
				$(".messager-window").css("z-index",10000);
			}
		}
	});

	$("#repairItemModifyButton").click(function(){ 
		if ($("#repairItemEditForm").form("validate")) {
			$("#repairItemEditForm").form({
			    url:"RepairItem/" +  $("#repairItem_itemId_edit").val() + "/update",
			    onSubmit: function(){
					if($("#repairItemEditForm").form("validate"))  {
	                	$.messager.progress({
							text : "正在提交数据中...",
						});
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                	var obj = jQuery.parseJSON(data);
                    if(obj.success){
                        $.messager.alert("消息","信息修改成功！");
                        $(".messager-window").css("z-index",10000);
                        //location.href="frontlist";
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    } 
			    }
			});
			//提交表单
			$("#repairItemEditForm").submit();
		} else {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		}
	});
});
