﻿$(function () {
	$.ajax({
		url : "Live/" + $("#live_liveId_edit").val() + "/update",
		type : "get",
		data : {
			//liveId : $("#live_liveId_edit").val(),
		},
		beforeSend : function () {
			$.messager.progress({
				text : "正在获取中...",
			});
		},
		success : function (live, response, status) {
			$.messager.progress("close");
			if (live) { 
				$("#live_liveId_edit").val(live.liveId);
				$("#live_liveId_edit").validatebox({
					required : true,
					missingMessage : "请输入记录id",
					editable: false
				});
				$("#live_buildingObj_buildingId_edit").combobox({
					url:"Building/listAll",
					valueField:"buildingId",
					textField:"buildingName",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#live_buildingObj_buildingId_edit").combobox("select", live.buildingObjPri);
						//var data = $("#live_buildingObj_buildingId_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#live_buildingObj_buildingId_edit").combobox("select", data[0].buildingId);
						//}
					}
				});
				$("#live_roomNo_edit").val(live.roomNo);
				$("#live_roomNo_edit").validatebox({
					required : true,
					missingMessage : "请输入入住宿舍号",
				});
				$("#live_studentObj_studentNo_edit").combobox({
					url:"Student/listAll",
					valueField:"studentNo",
					textField:"name",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#live_studentObj_studentNo_edit").combobox("select", live.studentObjPri);
						//var data = $("#live_studentObj_studentNo_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#live_studentObj_studentNo_edit").combobox("select", data[0].studentNo);
						//}
					}
				});
				$("#live_inDate_edit").datebox({
					value: live.inDate,
					required: true,
					showSeconds: true,
				});
				$("#live_liveMemo_edit").val(live.liveMemo);
			} else {
				$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
				$(".messager-window").css("z-index",10000);
			}
		}
	});

	$("#liveModifyButton").click(function(){ 
		if ($("#liveEditForm").form("validate")) {
			$("#liveEditForm").form({
			    url:"Live/" +  $("#live_liveId_edit").val() + "/update",
			    onSubmit: function(){
					if($("#liveEditForm").form("validate"))  {
	                	$.messager.progress({
							text : "正在提交数据中...",
						});
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                	var obj = jQuery.parseJSON(data);
                    if(obj.success){
                        $.messager.alert("消息","信息修改成功！");
                        $(".messager-window").css("z-index",10000);
                        //location.href="frontlist";
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    } 
			    }
			});
			//提交表单
			$("#liveEditForm").submit();
		} else {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		}
	});
});
