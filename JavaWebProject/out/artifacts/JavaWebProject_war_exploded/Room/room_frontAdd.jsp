﻿<%@ page language="java" import="java.util.*"  contentType="text/html;charset=UTF-8"%> 
<%@ page import="com.org.po.Building" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1 , user-scalable=no">
<title>宿舍添加</title>
<link href="<%=basePath %>plugins/bootstrap.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-dashen.css" rel="stylesheet">
<link href="<%=basePath %>plugins/font-awesome.css" rel="stylesheet">
<link href="<%=basePath %>plugins/animate.css" rel="stylesheet">
<link href="<%=basePath %>plugins/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body style="margin-top:70px;">
<div class="container">
<jsp:include page="../header.jsp"></jsp:include>
	<div class="col-md-12 wow fadeInLeft">
		<ul class="breadcrumb">
  			<li><a href="<%=basePath %>index.jsp">首页</a></li>
  			<li><a href="<%=basePath %>Room/frontlist">宿舍管理</a></li>
  			<li class="active">添加宿舍</li>
		</ul>
		<div class="row">
			<div class="col-md-10">
		      	<form class="form-horizontal" name="roomAddForm" id="roomAddForm" enctype="multipart/form-data" method="post"  class="mar_t15">
				  <div class="form-group">
				  	 <label for="room_buildingObj_buildingId" class="col-md-2 text-right">所在公寓楼:</label>
				  	 <div class="col-md-8">
					    <select id="room_buildingObj_buildingId" name="room.buildingObj.buildingId" class="form-control">
					    </select>
				  	 </div>
				  </div>
				  <div class="form-group">
				  	 <label for="room_roomNo" class="col-md-2 text-right">宿舍号:</label>
				  	 <div class="col-md-8">
					    <input type="text" id="room_roomNo" name="room.roomNo" class="form-control" placeholder="请输入宿舍号">
					 </div>
				  </div>
				  <div class="form-group">
				  	 <label for="room_roomPhoto" class="col-md-2 text-right">宿舍照片:</label>
				  	 <div class="col-md-8">
					    <img  class="img-responsive" id="room_roomPhotoImg" border="0px"/><br/>
					    <input type="hidden" id="room_roomPhoto" name="room.roomPhoto"/>
					    <input id="roomPhotoFile" name="roomPhotoFile" type="file" size="50" />
				  	 </div>
				  </div>
				  <div class="form-group">
				  	 <label for="room_personNum" class="col-md-2 text-right">床位数:</label>
				  	 <div class="col-md-8">
					    <input type="text" id="room_personNum" name="room.personNum" class="form-control" placeholder="请输入床位数">
					 </div>
				  </div>
				  <div class="form-group">
				  	 <label for="room_roomDesc" class="col-md-2 text-right">房间详情:</label>
				  	 <div class="col-md-8">
							    <textarea name="room.roomDesc" id="room_roomDesc" style="width:100%;height:500px;"></textarea>
					 </div>
				  </div>
		          <div class="form-group">
		             <span class="col-md-2""></span>
		             <span onclick="ajaxRoomAdd();" class="btn btn-primary bottom5 top5">添加</span>
		          </div> 
		          <style>#roomAddForm .form-group {margin:5px;}  </style>  
				</form> 
			</div>
			<div class="col-md-2"></div> 
	    </div>
	</div>
</div>
<jsp:include page="../footer.jsp"></jsp:include> 
<script src="<%=basePath %>plugins/jquery.min.js"></script>
<script src="<%=basePath %>plugins/bootstrap.js"></script>
<script src="<%=basePath %>plugins/wow.min.js"></script>
<script src="<%=basePath %>plugins/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<%=basePath %>plugins/bootstrap-datetimepicker.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="<%=basePath %>plugins/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/ueditor1_4_3/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/ueditor1_4_3/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="${pageContext.request.contextPath}/ueditor1_4_3/lang/zh-cn/zh-cn.js"></script>
<script>
//实例化编辑器
var room_roomDesc_editor = UE.getEditor('room_roomDesc'); //房间详情编辑器
var basePath = "<%=basePath%>";
	//提交添加宿舍信息
	function ajaxRoomAdd() { 
		//提交之前先验证表单
		$("#roomAddForm").data('bootstrapValidator').validate();
		if(!$("#roomAddForm").data('bootstrapValidator').isValid()){
			return;
		}
		if(room_roomDesc_editor.getContent() == "") {
			alert('房间详情不能为空');
			return;
		}
		jQuery.ajax({
			type : "post",
			url : basePath + "Room/add",
			dataType : "json" , 
			data: new FormData($("#roomAddForm")[0]),
			success : function(obj) {
				if(obj.success){ 
					alert("保存成功！");
					$("#roomAddForm").find("input").val("");
					$("#roomAddForm").find("textarea").val("");
					room_roomDesc_editor.setContent("");
				} else {
					alert(obj.message);
				}
			},
			processData: false, 
			contentType: false, 
		});
	} 
$(function(){
	/*小屏幕导航点击关闭菜单*/
    $('.navbar-collapse a').click(function(){
        $('.navbar-collapse').collapse('hide');
    });
    new WOW().init();
	//验证宿舍添加表单字段
	$('#roomAddForm').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			"room.roomNo": {
				validators: {
					notEmpty: {
						message: "宿舍号不能为空",
					}
				}
			},
			"room.personNum": {
				validators: {
					notEmpty: {
						message: "床位数不能为空",
					},
					integer: {
						message: "床位数不正确"
					}
				}
			},
		}
	}); 
	//初始化所在公寓楼下拉框值
	$.ajax({
		url: basePath + "Building/listAll",
		type: "get",
		success: function(buildings,response,status) { 
			$("#room_buildingObj_buildingId").empty();
			var html="";
    		$(buildings).each(function(i,building){
    			html += "<option value='" + building.buildingId + "'>" + building.buildingName + "</option>";
    		});
    		$("#room_buildingObj_buildingId").html(html);
    	}
	});
})
</script>
</body>
</html>
