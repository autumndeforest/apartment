/*
 * 自己的JS脚本
 * @Date:   2019-10-31 10:59:26
 * @Last Modified by:   chenyq
 */

'use strict';

$(function() {
  // 当文档加载完成才会执行
  /**
   * 根据屏幕宽度的变化决定轮播图片应该展示什么
   * @return {[type]} [description]
   */
  function resize() {
    // 获取屏幕宽度
    var windowWidth = $(window).width();
    // 判断屏幕属于大还是小
    var isSmallScreen = windowWidth < 768;
    // 根据大小为界面上的每一张轮播图设置背景
    // $('#main_ad > .carousel-inner > .item') // 获取到的是一个DOM数组（多个元素）
    $('#main_ad > .carousel-inner > .item').each(function(i, item) {
      // 因为拿到是DOM对象 需要转换
      var $item = $(item);
      // var imgSrc = $item.data(isSmallScreen ? 'image-xs' : 'image-lg');
      var imgSrc =
        isSmallScreen ? $item.data('image-xs') : $item.data('image-lg');

      // 设置背景图片
      $item.css('backgroundImage', 'url("' + imgSrc + '")');
      //
      // 因为我们需要小图时 尺寸等比例变化，所以小图时我们使用img方式
      if (isSmallScreen) {
        $item.html('<img src="' + imgSrc + '" alt="" />');
      } else {
        $item.empty();
      }
    });
  }

  $(window).on('resize', resize).trigger('resize');

  // 1. 获取手指在轮播图元素上的一个滑动方向（左右）
  // 获取界面上的轮播图容器
  var $carousels = $('.carousel');
  var startX, endX;
  var offset = 50;
  // 注册滑动事件
  $carousels.on('touchstart', function(e) {
    // 手指触摸开始时记录一下手指所在的坐标X
    startX = e.originalEvent.touches[0].clientX;
    // console.log(startX);
  });

  $carousels.on('touchmove', function(e) {
    // 变量重复赋值
    endX = e.originalEvent.touches[0].clientX;
    // console.log(endX);
  });
  $carousels.on('touchend', function(e) {
    console.log(e);
    // 结束触摸一瞬间记录最后的手指所在坐标X
    // 比大小
    // console.log(endX);
    // 控制精度
    // 获取每次运动的距离，当距离大于一定值时认为是有方向变化
    var distance = Math.abs(startX - endX);
    if (distance > offset) {
      // 有方向变化
      // console.log(startX > endX ? '←' : '→');
      // 2. 根据获得到的方向选择上一张或者下一张
      //     - $('a').click();
      //     - 原生的carousel方法实现 http://v3.bootcss.com/javascript/#carousel-methods
      $(this).carousel(startX > endX ? 'next' : 'prev');
    }
  });

  /*小屏幕导航点击关闭菜单*/
  $('.navbar-collapse a').click(function(){
  	$(this).css("background","lightgreen");
      $('.navbar-collapse').collapse('hide');
  });
  new WOW().init();

});
//滚动图
window.onload=function(){
    var oDiv=document.getElementById('div1');
    var oUl=oDiv.getElementsByTagName('ul')[0];
    var aLi=oUl.getElementsByTagName('li');
    var aA=document.getElementsByTagName('abbr');//获取向右向左的箭头
    var timer=null;
    var iSpeed=1;
    oUl.innerHTML+=oUl.innerHTML;//定义图片可以循环播放
    oUl.style.width=aLi.length*aLi[0].offsetWidth+'px';//定义外层ul的宽度，根据图片的个数和每个图片的宽度计算，保证总宽度是可调整的
    function fnMove(){
        if(oUl.offsetLeft<-oUl.offsetWidth/2){
            oUl.style.left=0;
        }else if(oUl.offsetLeft>0){
            oUl.style.left=-oUl.offsetWidth/2+'px';
        }//定义到边界的时候，实现无缝衔接

        oUl.style.left=oUl.offsetLeft+iSpeed+'px';

//定义图片的右边距随着速度不断不断增加，或减小，实现运动的效果
    }
    timer=setInterval(fnMove,30);
    aA[0].onclick=function(){
        iSpeed=-2;
//按下左箭头，定义向左运动

    }
    aA[1].onclick=function(){
        iSpeed=2;
//按下右箭头，定义向右运动
    }
    oDiv.onmouseover=function(){
        clearInterval(timer);

//鼠标移动到图片上，清除定时器，停止运动
    }
    oDiv.onmouseout=function(){
        timer=setInterval(fnMove,30);

//鼠标移出，重新开启定时器，重新运动
    }
};

//图片浏览器




