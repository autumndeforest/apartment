﻿$(function () {
	$.ajax({
		url : "Repair/" + $("#repair_repairId_edit").val() + "/update",
		type : "get",
		data : {
			//repairId : $("#repair_repairId_edit").val(),
		},
		beforeSend : function () {
			$.messager.progress({
				text : "正在获取中...",
			});
		},
		success : function (repair, response, status) {
			$.messager.progress("close");
			if (repair) { 
				$("#repair_repairId_edit").val(repair.repairId);
				$("#repair_repairId_edit").validatebox({
					required : true,
					missingMessage : "请输入报修id",
					editable: false
				});
				$("#repair_buildingObj_buildingId_edit").combobox({
					url:"Building/listAll",
					valueField:"buildingId",
					textField:"buildingName",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#repair_buildingObj_buildingId_edit").combobox("select", repair.buildingObjPri);
						//var data = $("#repair_buildingObj_buildingId_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#repair_buildingObj_buildingId_edit").combobox("select", data[0].buildingId);
						//}
					}
				});
				$("#repair_roomNo_edit").val(repair.roomNo);
				$("#repair_roomNo_edit").validatebox({
					required : true,
					missingMessage : "请输入宿舍号",
				});
				$("#repair_repairItemObj_itemId_edit").combobox({
					url:"RepairItem/listAll",
					valueField:"itemId",
					textField:"itemName",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#repair_repairItemObj_itemId_edit").combobox("select", repair.repairItemObjPri);
						//var data = $("#repair_repairItemObj_itemId_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#repair_repairItemObj_itemId_edit").combobox("select", data[0].itemId);
						//}
					}
				});
				$("#repair_repairDesc_edit").val(repair.repairDesc);
				$("#repair_repairDesc_edit").validatebox({
					required : true,
					missingMessage : "请输入问题描述",
				});
				$("#repair_studentObj_studentNo_edit").combobox({
					url:"Student/listAll",
					valueField:"studentNo",
					textField:"name",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#repair_studentObj_studentNo_edit").combobox("select", repair.studentObjPri);
						//var data = $("#repair_studentObj_studentNo_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#repair_studentObj_studentNo_edit").combobox("select", data[0].studentNo);
						//}
					}
				});
				$("#repair_addTime_edit").datetimebox({
					value: repair.addTime,
					required: true,
					showSeconds: true,
				});
				$("#repair_repairStateObj_stateId_edit").combobox({
					url:"RepairState/listAll",
					valueField:"stateId",
					textField:"stateName",
					panelHeight: "auto",
					editable: false, //不允许手动输入 
					onLoadSuccess: function () { //数据加载完毕事件
						$("#repair_repairStateObj_stateId_edit").combobox("select", repair.repairStateObjPri);
						//var data = $("#repair_repairStateObj_stateId_edit").combobox("getData"); 
						//if (data.length > 0) {
							//$("#repair_repairStateObj_stateId_edit").combobox("select", data[0].stateId);
						//}
					}
				});
				$("#repair_handleResult_edit").val(repair.handleResult);
			} else {
				$.messager.alert("获取失败！", "未知错误导致失败，请重试！", "warning");
				$(".messager-window").css("z-index",10000);
			}
		}
	});

	$("#repairModifyButton").click(function(){ 
		if ($("#repairEditForm").form("validate")) {
			$("#repairEditForm").form({
			    url:"Repair/" +  $("#repair_repairId_edit").val() + "/update",
			    onSubmit: function(){
					if($("#repairEditForm").form("validate"))  {
	                	$.messager.progress({
							text : "正在提交数据中...",
						});
	                	return true;
	                } else {
	                    return false;
	                }
			    },
			    success:function(data){
			    	$.messager.progress("close");
                	var obj = jQuery.parseJSON(data);
                    if(obj.success){
                        $.messager.alert("消息","信息修改成功！");
                        $(".messager-window").css("z-index",10000);
                        //location.href="frontlist";
                    }else{
                        $.messager.alert("消息",obj.message);
                        $(".messager-window").css("z-index",10000);
                    } 
			    }
			});
			//提交表单
			$("#repairEditForm").submit();
		} else {
			$.messager.alert("错误提示","你输入的信息还有错误！","warning");
			$(".messager-window").css("z-index",10000);
		}
	});
});
